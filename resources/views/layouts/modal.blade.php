  {{-- Modal Form Edit profil--}}
 <div id="edt"class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="editpr" class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2"for="id_prof">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="rid" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="slogan">Slogan</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="slogan"  name="slogan">
            <p class="errorSl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="highlight">Highlight</label>
            <div class="col-sm-10">
            <textarea type="name" class="form-control" id="highlight" ></textarea>
            <p class="errorHi text-center alert alert-danger hidden"></p>
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-sm-2"for="profil">Profil</label>
            <div class="col-sm-10">
            <textarea type="name" class="form-control" id="profil" style="resize:none;height:300px"></textarea>
            <p class="errorPro text-center alert alert-danger hidden"></p>
            </div>
          </div>
        </form>
               
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal">
          <span id="footer_action_profil" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
 {{-- Modal Form Create kontak --}}
<div id="create" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="formu" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="jnskontak">Jenis:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jenis" name="jnskontak"
              placeholder="Isi Jenis"  required>
              <p class="errorjns text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="dtlkontak">Kontak :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="kontak" name="dtlkontak"
              placeholder="Isi Kontak"  required>
              
              <p class="errorkon text-center alert alert-danger hidden"></p>

            </div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-sm-2" for="ikon">Ikon :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="icon" name="ikon"
              placeholder="Isi Ikon"  required>
              
              <p class="errorIkon text-center alert alert-danger hidden"></p>

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="urut">Urut :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="urutt" name="urut"
              placeholder="Isi Urut"  required>
              
              <p class="errorurut text-center alert alert-danger hidden"></p>

            </div>
          </div>
        </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="add">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>


{{-- Modal Form Edit kontak--}}
 <div id="edtknk" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubah" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_ktk">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="fid" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="jnskontak">Jenis</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="jns" >
            <p class="errorjns text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="dtlkontak">Kontak</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="kntk" >
            <p class="errorkon text-center alert alert-danger hidden"></p>
            </div>
          </div><div class="form-group row add">
            <label class="control-label col-sm-2"for="ikon">Ikon</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="ikn" >
            <p class="errorIkon text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="urut">Urut</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="urt" >
            <p class="errorurut text-center alert alert-danger hidden"></p>
            </div>
          </div>
         
        </form>
            <!-- {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_ktk"></span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnko" data-dismiss="modal">
          <span id="footer_action_kontak" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span  id="footer_action_batal" class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete kontak -->
 <div id="hpsknt" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
           
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_ktk"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnk" data-dismiss="modal">
          <span id="footer_action_hapusknt" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span  class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
{{-- Modal Form Create Media Sosial --}}
<div id="buatsosmed" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ssd" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="jnssosmed">Jenis:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jenissosmed" name="jnssosmed"
              placeholder="Isi Jenis"  required>
              <p class="errorjns text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="dtlsosmed">Sosmed:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="sosmed" name="dtlsosmed"
              placeholder="Isi Sosmed"  required>
              
              <p class="errorsos text-center alert alert-danger hidden"></p>

            </div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-sm-2" for="ikonsosmed">Ikon :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="iconsosmed" name="ikonsosmed"
              placeholder="Isi Ikon"  required>
              
              <p class="errorIkon text-center alert alert-danger hidden"></p>

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="urutsosmed">Urut :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="urutsosmed" name="urutsosmed"
              placeholder="Isi Urut"  required>
              
              <p class="errorurut text-center alert alert-danger hidden"></p>

            </div>
          </div>
        </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahsos">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>
{{-- Modal Form Edit Sosmed--}}
 <div id="edtsm"class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahsos" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_sosmed">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="sid" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="jnssosmed">Jenis</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="jnssmd" >
            <p class="errorjns text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="dtlsosmed">Sosmed</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="media" >
            <p class="errorkon text-center alert alert-danger hidden"></p>
            </div>
          </div><div class="form-group row add">
            <label class="control-label col-sm-2"for="ikonsosmed">Ikon</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="iknsmd" >
            <p class="errorIkon text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="urutsosmed">Urut</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="urtsmd" >
            <p class="errorurut text-center alert alert-danger hidden"></p>
            </div>
          </div>
         
        </form>
          <!--   {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_sosmed"></span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnsos" data-dismiss="modal">
          <span id="footer_action_sosmed" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete sosmed -->
 <div id="hpsmd" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
           
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_sosmed"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtns" data-dismiss="modal">
          <span id="footer_action_hapusmd" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

{{-- Modal Form Create Office --}}
<div id="offc" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="kantor" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="nama">Nama:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="nama" name="nama"
              placeholder="Isi Nama"  required>
              <p class="errornama text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="alamat">Alamat :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="address" name="alamat"
              placeholder="Isi Alamat"  required>
              
              <p class="erroral text-center alert alert-danger hidden"></p>

            </div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-sm-2" for="kota">Kota :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="city" name="kota"
              placeholder="Isi Kota"  required>
              
              <p class="errorkota text-center alert alert-danger hidden"></p>

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="kodepos">Kode Pos :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="kp" name="kodepos"
              placeholder="Isi Kode Pos"  required>
              
              <p class="errorpos text-center alert alert-danger hidden"></p>

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="koordinat">Koordinat :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="kdnt" name="koordinat"
              placeholder="Isi Koordinat"  required>
              
              <p class="errorkdnt text-center alert alert-danger hidden"></p>

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="telp">Telp :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="telph" name="telp"
              placeholder="Isi Koordinat"  required>
              
              <p class="errortlp text-center alert alert-danger hidden"></p>

            </div>

          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="aktif">Aktif :</label>
            <!-- <div class="col-sm-10">
              <input type="text" class="form-control" id="aktf" name="aktif"   placeholder="Aktif" >
              
              <p class="errorakf text-center alert alert-danger hidden"></p>

            </div> -->
             <div class="col-sm-10">
            <input type="checkbox" class="form-check-input" id="aktf" value="1">
            </div>

          </div>
          <!-- <div class="form-group">
         
            <label class="control-label col-sm-2" for="aktif">Aktif :</label>
              <div class="col-sm-10">
              
              <input type="checkbox" class="aktif" id="aktf" name="aktif">
            
              <p class="erroraktf text-center alert alert-danger hidden"></p>

            </div>
          </div> -->
        </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahkantor">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>
{{-- Modal Form Edit Office--}}
 <div id="edtoff" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahofc" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_off">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="oid" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="nama">Nama</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="nm" >
            <p class="errorna text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="alamat">Alamat</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="almt" >
            <p class="erroral text-center alert alert-danger hidden"></p>
            </div>
          </div><div class="form-group row add">
            <label class="control-label col-sm-2"for="kota">Kota</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="kt" >
            <p class="errorkota text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="kodepos">Kode Pos</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="kodpos" >
            <p class="error text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="koordinat">Koordinat</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="krdt" >
            <p class="errorkdnt text-center alert alert-danger hidden"></p>
            </div>
          </div>
         
         <div class="form-group row add">
            <label class="control-label col-sm-2"for="telp">Telp</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="phone" >
            <p class="errortlp text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add">
            <label class="control-label col-sm-2"for="aktif">Aktif </label>
           <!--  <div class="col-sm-10">
            <input type="text" class="form-control" id="akf" >
            <p class="erroraktftext-center alert alert-danger hidden"></p>
            </div> -->
             <div class="col-sm-10">
            <input type="checkbox" class="form-check-input" id="akfedit" value="1">
            </div>
          </div>
         
         
         
        </form>
            <!-- {{-- Form Delete Office --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_off"></span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnn" data-dismiss="modal">
          <span id="footer_action_office" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete office -->
 <div id="hpsof" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_off"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtno" data-dismiss="modal">
          <span id="footer_action_hapusoff" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

 {{-- Modal Form Create Jenis --}}
<div id="mjenis" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="baru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="jps">Jenis:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jenis" name="jps"
              placeholder="Isi Jenis"  required>
              <p class="errorjns text-center alert alert-danger hidden"></p>
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahjenis">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>


{{-- Modal Form Edit Jenis--}}
 <div id="edtjns" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahjps" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_jps">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jid" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="jps">Jenis</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="jnsp" >
            <p class="errorjps text-center alert alert-danger hidden"></p>
            </div>
          </div>
        
        </form>
           <!--  {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_jps"></span>
        </div>   -->  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnj" data-dismiss="modal">
          <span id="footer_action_jenis" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete jenis -->
 <div id="hpsjn" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
          
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_jps"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnhj" data-dismiss="modal">
          <span id="footer_action_hpsjns" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
 {{-- Modal Form Create video --}}
<div id="mvid" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="linkbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="judulvid">Judul:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jdl" name="judulvid"
              placeholder="Isi Jenis"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="linkvid">Link:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="lnk" name="linkvid"
              placeholder="Isi Jenis"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahvid">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>
{{-- Modal Form Edit video--}}
 <div id="edtvd" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahvd" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_jps">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jdid" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="judulvid">Judul</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="jdlv" >
            <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="link">Link</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="lnkv" >
            <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
        
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnv" data-dismiss="modal">
          <span id="footer_action_video" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete video -->
 <div id="hpsvd" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnvd" data-dismiss="modal">
          <span id="footer_action_hapus" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
{{-- Modal Form Create Slider --}}
<div id="mslider" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="sliderbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="judulsld">Judul:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jdlsld" name="judulsld"
              placeholder="Judul"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="subjudul">Subjudul:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="sub" name="subjudul"
              placeholder="Subjudul"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="prakata">Prakata:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="pra" name="prakata"
              placeholder="Prakata"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="deskripsi">Deskripsi:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="dsk" name="deskripsi"
              placeholder="Prakata"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="posisisld">Posisi:</label>
          <div class="col-sm-10">
            <label class="radio-inline">
            <input type="radio" id="kanansld" name="posisisld" value="col-md-6 col-md-pull-6 hidden-xs:col-md-push-6">Kanan
            </label>
             <label class="radio-inline">
             <input type="radio" id="kirisld" name="posisisld" value="col-md-6 hidden-xs:text-right-md">Kiri
            </label>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="urut">Urut:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="urutan" name="urut"
              placeholder="Urut"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="aktif">Aktif:</label>
            <div class="col-sm-10">
            <input type="checkbox" class="form-check-input" id="aktipsld" value="1">
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahslider">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remoev"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>

{{-- Modal Form Edit slider--}}
 <div id="edtsli" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahsl" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_sld">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="sld_id" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="judulsld">Judul</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="jsld" >
            <p class="errorjsl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="subjudul">Subjudul</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="subjdl" >
            <p class="errorsub text-center alert alert-danger hidden"></p>
            </div>
          </div>
           <div class="form-group row add" >
            <label class="control-label col-sm-2"for="prakata">Prakata</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="prkt" >
            <p class="errorpra text-center alert alert-danger hidden"></p>
            </div>
          </div> 
           <div class="form-group row add" >
            <label class="control-label col-sm-2"for="deskripsi">Deskripsi</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="des" >
            <p class="errordes text-center alert alert-danger hidden"></p>
            </div>
          </div> 
         
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="posisi">Posisi</label>
            <div class="col-sm-10">
              <label class="radio-inline">
            <input type="radio" id="kananedit" name="posisiedit" value="col-md-6 col-md-pull-6 hidden-xs:col-md-push-6">Kanan
            </label>
             <label class="radio-inline">
             <input type="radio" id="kiriedit" name="posisiedit" value="col-md-6 hidden-xs:text-right-md">Kiri
            </label>
            </div>
          </div>
          
           <div class="form-group row add" >
            <label class="control-label col-sm-2"for="urut">Urut</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="ur" >
            <p class="errorurt text-center alert alert-danger hidden"></p>
            </div>
          </div>
           <div class="form-group row add" >

            <label class="control-label col-sm-2"for="aktif">Aktif</label>
           <div class="col-sm-10">
            <input type="checkbox" class="form-check-input" id="aktipedit" value="1">
            </div>
          </div>
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnl" data-dismiss="modal">
          <span id="footer_action_slider" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete slider -->
 <div id="hpssl" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_sld"><vid/span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnd" data-dismiss="modal">
          <span id="footer_action_hpsslid" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- Modal pICTURE -->
 <!--  <div class="modal fade" id="pic" role="dialog">
    <div class="modal-dialog">
     -->
      <!-- Modal content-->
     <!--  <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Picture</h4>
        </div>
        <div class="modal-body">
        <img src="" id="pctr">
                   
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div> -->

{{-- Modal Form Create Produk --}}
<div id="mprod" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="prdkbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="jps">Jenis:</label>
            <div class="col-sm-10">
            <select  class="form-control"  id="jpsr" name="jps" >
             @if(count($jenis)>0)
              @foreach($jenis->all() as $jen)
               <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
              @endforeach
            @endif
              
            </select>
              <!-- input type="text" class="form-control" id="jpsr" name="jps"
              placeholder="Isi Jenis"  required> -->
             <!--  <p class="errorjns text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="produk">Produk:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="pdk" name="produk"
              placeholder="Isi Produk"  required>
              <!-- <p class="errorlnk text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="deskripsi">Deskripsi:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="desk" name="deskripsi"
              placeholder="Isi Deskripsi"  required>
              <!-- <p class="errorlnk text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahpro">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>
{{-- Modal Form Edit Produk--}}
 <div id="modalpro" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="produkedit" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_prod">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="pro_id" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="jps">Jenis</label>
            <div class="col-sm-10">
              <select  class="form-control"  id="jspv" name="jps" >
             @if(count($jenis)>0)
              @foreach($jenis->all() as $jen)
               <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="Produk">Produk</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="prk" >
           <!--  <p class="errorsub text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
           <div class="form-group row add" >
            <label class="control-label col-sm-2"for="deskripsi">Deskripsi</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="dk" >
            <!-- <p class="errorpra text-center alert alert-danger hidden"></p> -->
            </div>
          </div> 
         
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtng" data-dismiss="modal">
          <span id="footer_action_produk" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete produk -->
 <div id="produkhps" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete produk --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_prod"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_hapusproduk" data-dismiss="modal">
          <span id="footer_action_hpsprod" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
{{-- Modal Form Create Projek --}}
<div id="mproj" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="prjkbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="jps">Jenis:</label>
            <div class="col-sm-10">
            <select  class="form-control"  id="jpsl" name="jps" >
             @if(count($jenis)>0)
              @foreach($jenis->all() as $jen)
               <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
              @endforeach
            @endif
              
            </select>
              <!-- input type="text" class="form-control" id="jpsr" name="jps"
              placeholder="Isi Jenis"  required> -->
             <!--  <p class="errorjns text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="projek">Projek:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="pjk" name="Projek"
              placeholder="Isi Projek"  required>
              <!-- <p class="errorlnk text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="deskripsi">Deskripsi:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="deskr" name="deskripsi"
              placeholder="Isi Deskripsi"  required>
              <!-- <p class="errorlnk text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahprj">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>
{{-- Modal Form Edit Projek--}}
 <div id="modalprj" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="projekedit" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_prjt">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="prj_id" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="jps">Jenis</label>
            <div class="col-sm-10">
              <select  class="form-control"  id="jsps" name="jps" >
             @if(count($jenis)>0)
              @foreach($jenis->all() as $jen)
               <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="projek">Projek</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="prjk" >
           <!--  <p class="errorsub text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
           <div class="form-group row add" >
            <label class="control-label col-sm-2"for="deskripsi">Deskripsi</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="dr" >
            <!-- <p class="errorpra text-center alert alert-danger hidden"></p> -->
            </div>
          </div> 
         
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnb" data-dismiss="modal">
          <span id="footer_action_projek" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete Projek -->
 <div id="projekhps" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Projek --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_prjt"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_hapusprojek" data-dismiss="modal">
          <span id="footer_action_hpsprjt" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
{{-- Modal Form Create Servis --}}
<div id="mser" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="servbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="jps">Jenis:</label>
            <div class="col-sm-10">
            <select  class="form-control"  id="jpss" name="jps" >
             @if(count($jenis)>0)
              @foreach($jenis->all() as $jen)
               <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
              @endforeach
            @endif
              
            </select>
              <!-- input type="text" class="form-control" id="jpsr" name="jps"
              placeholder="Isi Jenis"  required> -->
             <!--  <p class="errorjns text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="servis">Servis:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="srs" name="servis"
              placeholder="Isi Servis"  required>
              <!-- <p class="errorlnk text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="ikon">Ikon:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in" name="ikon"
              placeholder="Isi Ikon"  required>
              <!-- <p class="errorlnk text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahsrs">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>
{{-- Modal Form Edit Servis--}}
 <div id="modalsrv" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="servisedit" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_serv">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="serv_id" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="jps">Jenis</label>
            <div class="col-sm-10">
              <select  class="form-control"  id="jsk" name="jps" >
             @if(count($jenis)>0)
              @foreach($jenis->all() as $jen)
               <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="servis">Servis</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="srv" >
           <!--  <p class="errorsub text-center alert alert-danger hidden"></p> -->
            </div>
          </div>
           <div class="form-group row add" >
            <label class="control-label col-sm-2"for="ikon">Ikon</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="io" >
            <!-- <p class="errorpra text-center alert alert-danger hidden"></p> -->
            </div>
          </div> 
         
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnr" data-dismiss="modal">
          <span id="footer_action_servis" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- delete servis -->
 <div id="servishps" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Servis --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_serv"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_hapusservis" data-dismiss="modal">
          <span id="footer_action_hpsserv" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- //create Unduh -->
<div id="munduh" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="unduhbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="judulunduh">Judul Unduhan:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jdlundh" name="judulunduh"
              placeholder="Isi Judul unduhan"  required>
              <p class="errorjdln text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="linkunduh">Link Unduhan:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ud" name="linkunduh"
              placeholder="Isi Link Unduhan"  required>
              <p class="errorlnknd text-center alert alert-danger hidden"></p>
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahunduh">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>

{{-- Modal Form Edit Unduh--}}
 <div id="edtunduh" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahunduh" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_unduh">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="jdun" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="judulunduh">Judul Unduhan</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="jdlu" >
            <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="linkunduh">Link Unduhan</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="lnku" >
            <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
        
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnq" data-dismiss="modal">
          <span id="footer_action_unduh" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>
<!-- delete unduh -->
 <div id="hpsunduh" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Unduh --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_unduh"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnw" data-dismiss="modal">
          <span id="footer_action_hapusunduh" class="glyphicon"> Hapus</span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- //create User -->
<div id="musr" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="usrbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="namauser">Nama User:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="nmusr" name="namauser"
              placeholder="Isi Nama User"  required>
              <p class="errorjdln text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ema" name="email"
              placeholder="Isi Email"  required>
              <p class="errorlnknd text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" >Jenis User:</label>
            <div class="col-sm-10">
             <!--  <input type="text" class="form-control" id="jnsus" name="jnsuser"
              placeholder="Isi Jenis User"  required>
              <p class="errorjdln text-center alert alert-danger hidden"></p> -->
              
             <select  class="form-control"  id="jnsus" >
             @if(count($jenisuser)>0)
              @foreach($jenisuser->all() as $uw)
               <option value="{{$uw->id_jns}}">{{$uw->jenisuser}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="status">Status:</label>
            <div class="col-sm-10">
              <!-- <input type="text" class="form-control" id="st" name="status"
              placeholder="Isi Status"  required>
              <p class="errorlnknd text-center alert alert-danger hidden"></p> -->
              <input type="checkbox" class="form-check-input" id="st" value="1">
            
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahuser">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>


{{-- Modal Form Edit User--}}
 <div id="edtuser" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahuser" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_usr">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id_us" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="namauser">Nama User</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="nu" >
            <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="email">Email</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="maile" >
            <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
        <div class="form-group row add" >
            <label class="control-label col-sm-2"for="jenisuser">Jenis User</label>
            <div class="col-sm-10">
            <!-- <input type="name" class="form-control" id="jusr" >
            <p class="errorjdl text-center alert alert-danger hidden"></p> -->
             <select  class="form-control"  id="jusr" >
             @if(count($jenisuser)>0)
              @foreach($jenisuser->all() as $uw)
               <option value="{{$uw->id_jns}}">{{$uw->jenisuser}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="status">Status</label>
            <div class="col-sm-10">
            <!-- <input type="name" class="form-control" id="stat" >
            <p class="errorlnk text-center alert alert-danger hidden"></p> -->
            <input type="checkbox" class="form-check-input" id="stat" value="1">
            
            </div>
          </div>
        
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnus" data-dismiss="modal">
          <span id="footer_action_user" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>


<!-- delete user -->
 <div id="hpsusr" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete User --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_usr"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_usr" data-dismiss="modal">
          <span id="footer_action_hapususer" class="glyphicon"> Hapus</span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- //create Blog -->
<div id="mblog" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="blogbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="judul">Judul :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="judulblog" name="judul"
              placeholder="Isi Judul "  required>
              <p class="errorjdln text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="namauser">Penulis:</label>
            <div class="col-sm-10">
             <select  class="form-control"  id="nuser" name="namauser" >
             @if(count($user)>0)
              @foreach($user->all() as $us)
               <option value="{{$us->id_usr}}">{{$us->namauser}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
        
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahblog">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div><!-- 
{-- Modal Form Edit Blog--}} -->
 <div id="edtblog" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahuser" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id_blog">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id_blg" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="judul">Judul</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="jl" >
            <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="namauser">Penulis</label>
            <div class="col-sm-10">
            <select  class="form-control"  id="use" name="namauser" >
             @if(count($user)>0)
              @foreach($user->all() as $us)
               <option value="{{$us->id_usr}}">{{$us->namauser}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>

        
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnbg" data-dismiss="modal">
          <span id="footer_action_blog" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- delete blog -->
 <div id="hpsblg" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Blog --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_blog"><vid/span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_blg" data-dismiss="modal">
          <span id="footer_action_hapusblog" class="glyphicon"> Hapus</span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

 {{-- Modal Form Create Klien --}}
<div id="mklien" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="klienbaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="klien">Klein:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="kl" name="klien"
              placeholder="Isi Klien"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="alamat">Alamat:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="alm" name="alamat"
              placeholder="Isi Alamat"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
            <div class="form-group">
            <label class="control-label col-sm-2" for="telp">Telp:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tp" name="telp"
              placeholder="Isi Telp"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
            <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="mail" name="email"
              placeholder="Isi Klien"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahklien">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div><!-- 
{-- Modal Form Edit Klien--}} --> 
 <div id="edtklien" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahklien" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id_kl" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="klien">Klien</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="ki">
            <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="alamat">Alamat</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="mt" >
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="telp">Telp</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="lp">
            <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="email">Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="imel" >
            </div>
          </div>
        
        
        </form>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtnkl" data-dismiss="modal">
          <span id="footer_action_klien" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- delete klien -->
 <div id="hpskl" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Klien --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_klien"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_kln" data-dismiss="modal">
          <span id="footer_action_hapusklien" class="glyphicon"> Hapus</span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

 {{-- Modal Form Create Testi --}}
<div id="mtesti" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="testibaru" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="klien">Klien:</label>
            <div class="col-sm-10">
              <select  class="form-control"  id="ket" name="klien" >
             @if(count($klien)>0)
              @foreach($klien->all() as $kli)
               <option value="{{$kli->id_klien}}">{{$kli->klien}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="nama">Nama:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="na" name="nama"
              placeholder="Isi Nama"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
            <div class="form-group">
            <label class="control-label col-sm-2" for="testimoni">Testimoni:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ts" name="testimoni"
              placeholder="Isi Testimoni"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
            
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahtesti">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>

{{-- Modal Form Edit Testi--}} 
 <div id="edttesti" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahtesti" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id_ts" disabled>
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="klien">Klien</label>
            <div class="col-sm-10">
            <select  class="form-control"  id="ken" name="klien" >
             @if(count($klien)>0)
              @foreach($klien->all() as $kli)
               <option value="{{$kli->id_klien}}">{{$kli->klien}}</option>
              @endforeach
            @endif
              
            </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="nama">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ma" >
            </div>
          </div>
          <div class="form-group row add" >
            <label class="control-label col-sm-2"for="testimoni">Testimoni</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="tn">
            <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
        
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtntm" data-dismiss="modal">
          <span id="footer_action_testi" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- delete testi -->
 <div id="hpstes" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Testi --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_testi"></span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_tst" data-dismiss="modal">
          <span id="footer_action_hapustesti" class="glyphicon"> Hapus</span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

 {{-- Modal Form Create Partner --}}
<div id="mpart" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="partnerbaru" class="form-horizontal" role="form">
          <div class="form-group">
            <label class="control-label col-sm-2" for="partner">Partner:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="pp" name="partner"
              placeholder="Isi Partner"  required>
              <p class="errorlnk text-center alert alert-danger hidden"></p>
            </div>
          </div>
            <div class="form-group">
            <label class="control-label col-sm-2" for="alamat">Alamat:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="at" name="alamat"
              placeholder="Isi Alamat"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-sm-2" for="telp">Telp:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tep" name="telp"
              placeholder="Isi Telp"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ems" name="alaemailmat"
              placeholder="Isi email"  required>
              <p class="errorjdl text-center alert alert-danger hidden"></p>
            </div>
          </div>
            
         </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="tambahpartner">
              <span class="glyphicon glyphicon-plus"></span>Simpan
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remove"></span>Batal
            </button>
          </div>
    </div>
  </div>
</div>

{{-- Modal Form Edit Partner--}} 
 <div id="edtpartner" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="ubahpartner" class="form-horizontal" role="modal">
         <div class="form-group">
            <label class="control-label col-sm-2"for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ppt" disabled>
            </div>
          </div>
         
          <div class="form-group">
            <label class="control-label col-sm-2"for="partner">Partner</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="pt" >
            </div>
          </div>
          <div class="form-group " >
            <label class="control-label col-sm-2"for="alamat">Alamat</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="alt">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="telp">Telp</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tlp" >
            </div>
          </div>
          <div class="form-group " >
            <label class="control-label col-sm-2"for="email">Email</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="emaile">
            </div>
          </div>
        
        </form>
            <!-- {{-- Form Delete Video --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_vid"><vid/span>
        </div>    --> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtngt" data-dismiss="modal">
          <span id="footer_action_partner" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- delete partner -->
 <div id="hpspart" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete Partner --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_partner" id="idhapuspart"><vid/span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_prt" data-dismiss="modal">
          <span id="footer_action_hapuspartner" class="glyphicon"> Hapus</span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- upload file slider -->
<div  id="uploadslid" class="modal fade" id="pic" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Slider</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambarsld')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="picsld" id="picsld">
            <input type="file" name="picturesls">
            <center><img id="upgbr" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
  

<!-- show file Picture -->
<div  id="picshow" class="modal fade" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Info Gambar</h4>
        </div>
        <div class="modal-body">
       <center><img id="gmbr" src=""/></center>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
  

<!-- upload file produk -->
<div  id="uploadpro" class="modal fade" id="picproduk" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Produk</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambarproduk')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="picprd" id="picprd">
            <input type="file" name="pictureprdk">
            <center><img id="upgbrproduk" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
  


@if(count($produk)>0)
@foreach($produk as $prd)
<!-- show file produk -->
<div  id="produkshow-{{$prd->id_prod}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
    {{csrf_field()}}
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Info Gambar</h4>
        </div>
        <div class="modal-body">
        {{csrf_field()}}
       <!-- <center><img id="gbrproduk" src=""/></center> -->
       @if(count($gambar)>0)
        @foreach($gambar->where('id_prod', $prd->id_prod) as $gb)

        @if($gb->lokasi == null)
         <h4 align="center">Belum ada foto</h4>
        @else
        <center> <img id="gbrproduk" src="/grittekno/public/{{$gb->lokasi}}" ></center>
        @endif

        @endforeach
       @endif
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
@endif

<!-- upload file projek -->
<div  id="uploadprj" class="modal fade" id="picprojek" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Projek</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambarprojek')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="picprj" id="picprj">
            <input type="file" name="pictureprjk">
            <center><img id="upgbrprojek" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- 
//show blog -->
@if(count($projek)>0)
@foreach($projek as $pg)
<!-- show file produk -->
<div  id="projekshow-{{$pg->id_prjt}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
    {{csrf_field()}}
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Info Gambar</h4>
        </div>
        <div class="modal-body">
          {{csrf_field()}}
       <!-- <center><img id="gbrproduk" src=""/></center> -->
       @if(count($gambar)>0)
        @foreach($gambar->where('id_prjt', $pg->id_prjt) as $gb)

        @if($gb->lokasi == null)
         <h4 align="center">Belum ada foto</h4>
        @else
        <center> <img id="gbrshowprojek" src="/grittekno/public/{{$gb->lokasi}}" ></center>
        @endif

        @endforeach
       @endif
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
@endif
<!-- upload file userweb -->
<div  id="uploadusr" class="modal fade" id="pic" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload User WEB</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambarusr')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="picusr" id="picusr">
            <input type="file" name="pictureuser">
            <center><img id="upgbruser" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- 
//show blog -->
@if(count($blog)>0)
@foreach($blog as $bl)
<!-- show file produk -->
<div  id="blogshow-{{$bl->id_blog}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
    {{csrf_field()}}
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Info Gambar</h4>
        </div>
        <div class="modal-body">
        {{csrf_field()}}
       <!-- <center><img id="gbrproduk" src=""/></center> -->
       @if(count($gambar)>0)
        @foreach($gambar->where('id_blog', $bl->id_blog) as $gb)

        @if($gb->lokasi == null)
         <h4 align="center">Belum ada foto</h4>
        @else
        <center> <img id="gbrshowblog" src="/grittekno/public/{{$gb->lokasi}}" ></center>
        @endif

        @endforeach
       @endif
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
@endif
<!-- upload file Testimoni -->
<div  id="uploadtst" class="modal fade" id="pic" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Testimoni</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambartesti')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="pictst" id="pictst">
            <input type="file" name="picturetesti">
            <center><img id="upgbrtest" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- upload file Partner -->
<div  id="uploadprt" class="modal fade" id="pic" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Partner</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambarpartner')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="picprt" id="picprt">
            <input type="file" name="picturepartner">
            <center><img id="upgbrpart" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- upload file klien -->
<div  id="uploadkln" class="modal fade" id="pic" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Klien</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambarklien')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="pickln" id="pickln">
            <input type="file" name="pictureklien">
            <center><img id="upgbrkli" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- upload file blog -->
<div  id="uploadblg" class="modal fade" id="pic" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Blog</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="{{url('/uploadgambarblog')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="text" hidden="" name="picblg" id="picblg">
            <input type="file" name="pictureblog">
            <center><img id="upgbrblg" src=""/></center>
            <button type="submit" class="btn-success">Unggah</button>
       </form>
        </div>        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- delete Gambar -->
 <div id="gbrhapus" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete gambar produk --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_gbr"><vid/span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_hapusgambar" data-dismiss="modal">
          <span id="footer_action_hpsgbr" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div>

<!-- delete Gambar -->
 <!-- <div id="userpichapus" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
            {{-- Form Delete gambar userweb --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="name"></span>?
          <span class="hidden id_gbr"><vid/span>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn_hapususrgbr" data-dismiss="modal">
          <span id="footer_action_hpsgbrusr" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Batal
        </button>
      </div>
    </div>
  </div>
</div> -->