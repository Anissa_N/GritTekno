<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <title>Laravel Crud</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- toastr notifications -->
    {{-- <link rel="stylesheet" href="{{ asset('toastr/toastr.min.css') }}"> --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<style type="text/css">

.row-eq-height {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
}
#gmbr, #gbrprojek,#gbrshowblog,#gbrshowprojek{
  width:55%;
}

#gbrproduk,#gbrklien{
  width:65%;
}
</style>
  </head>
  <body>
<!-- <div class="container"> 
  @yield('content')
</div>
 -->

  <!-- toastr notifications -->

    <script type="text/javascript" src="{{ asset('/js/readmore.js') }}"></script> 
    {{-- <script type="text/javascript" src="{{ asset('toastr/toastr.min.js') }}"></script> --}}
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$('.profill').readmore();

$(document).ready( function () {
   var a = $('#knt').DataTable({
      
      "ajax":"{{url('data')}}",
       "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
          return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
        },
      },
      {"data":"id_ktk"},
      {"data":"jnskontak"},
      {"data":"dtlkontak"},
      {"data":"ikon"},
      {"data":"urut"},
      ],
      "order":[[0, 'asc']],
           
    });
a.on('order.dt search.dt', function() {
  a.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();


});
$(document).ready( function () {
var b = $('#sos').DataTable({
      "ajax":"{{url('datamedsos')}}",
       "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="editsos btn btn-warning btn-sm" data-id_sosmed="'+ a.id_sosmed +'" data-jnssosmed="'+ a.jnssosmed +'" data-dtlsosmed="'+ a.dtlsosmed +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-sos btn btn-danger btn-sm" data-id_sosmed="'+ a.id_sosmed +'" data-jnssosmed="'+ a.jnssosmed+'" data-dtlsosmed="'+ a.dtlsosmed +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
        },
      },
      {"data":"id_sosmed"},
      {"data":"jnssosmed"},
      {"data":"dtlsosmed"},
      {"data":"ikon"},
      {"data":"urut"},
     

            ],
              "order":[[0, 'asc']],
           
    });
b.on('order.dt search.dt', function() {
  b.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

      
});
$(document).ready( function () {
   var c = $('#office').DataTable({
      "ajax":"{{url('alam')}}",
        "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center", 
        render: function(a){
          return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edit-off btn btn-warning btn-sm" data-id_off="'+ a.id_off +'" data-nama="'+ a.nama +'" data-alamat="'+ a.alamat +'" data-kota="'+ a.kota +'" data-kodepos="'+ a.kodepos +'" data-koordinat="'+ a.koordinat +'" data-telp="'+ a.telp +'" data-aktif="'+ a.aktif +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-alm btn btn-danger btn-sm" data-id_off="'+ a.id_off +'" data-nama="'+ a.nama +'" data-alamat="'+ a.alamat +'" data-kota="'+ a.kota +'" data-kodepos="'+ a.kodepos +'" data-koordinat="'+ a.koordinat +'" data-telp="'+ a.telp +'" data-aktif="'+ a.aktif +'"> <i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
        },
      },
      {"data":"id_off"},
      {"data":"nama"},
      {"data":"alamat"},
      {"data":"kota"},
      {"data":"kodepos"},
      {"data":"koordinat"},
      {"data":"telp"},
      {"data":"aktif",
         render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="editor-active " disabled>';
                    }
                    return data;
                },
                className: "dt-body-center"
              }                 
            ],
            rowCallback: function ( row, data ) {
            // Set the checked state of the checkbox in the table
            $('input.editor-active', row).prop( 'checked', data.aktif == 1 );
        },
     "order":[[0, 'asc']],
           
    });
c.on('order.dt search.dt', function() {
  c.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

// $('#office').on( 'change', 'input.editor-active', function () {
//         editor
//             .edit( $(this).closest('tr'), false )
//             .set( 'aktif', $(this).prop( 'checked' ) ? 1 : 0 )
           
//     } );

});
//datatables jenis
$(document).ready( function () {
  var d =  $('#datajps').DataTable({
      
      "ajax":"{{url('jenis')}}",
       "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
          return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtjenis btn btn-warning btn-sm" data-id_jps="'+ a.id_jps +'" data-jps="'+ a.jps +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-jenis btn btn-danger btn-sm" data-id_jps="'+ a.id_jps +'" data-jps="'+ a.jps+'" ><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
        },
      },
      {"data":"id_jps"},
      {"data":"jps"},
            ],
           
            "order":[[0, 'asc']],
           
    });
d.on('order.dt search.dt', function() {
  d.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();


});
//datatables linkvideo
$(document).ready( function () {
   var e = $('#linkvd').DataTable({
      
      "ajax":"{{url('linkvid')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
          return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtlink btn btn-warning btn-sm" data-id_vid="'+ a.id_vid +'" data-judulvid="'+ a.judulvid +'"data-linkvid="'+ a.linkvid +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-link btn btn-danger btn-sm" data-id_vid="'+ a.id_vid +'" data-judulvid="'+ a.judulvid +'"data-linkvid="'+ a.linkvid +'" ><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
        },
      },
      {"data":"id_vid"},
      {"data":"judulvid"},
      {"data":"linkvid"},
            ],
            
     "order":[[0, 'asc']],
           
    });
e.on('order.dt search.dt', function() {
  e.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();


});
//datatables slider
$(document).ready( function () {
  var f = $('#slid').DataTable({
      
      "ajax":"{{url('dataslider')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
          return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edtsl btn btn-md" data-id_sld="'+ a.id_sld +'" data-judulsld="'+ a.judulsld +'"data-subjudul="'+ a.subjudul +'"data-prakata="'+ a.prakata +'"data-deskripsi="'+ a.deskripsi +'" data-posisi="'+ a.posisi +'"data-urut="'+ a.urut +'"data-aktif="'+ a.aktif +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-sl btn btn-md" data-id_sld="'+ a.id_sld +'" data-judulsld="'+ a.judulsld +'"data-subjudul="'+ a.subjudul +'"data-prakata="'+ a.prakata +'"data-deskripsi="'+ a.deskripsi +'" data-posisi="'+ a.posisi +'"data-urut="'+ a.urut +'"data-aktif="'+ a.aktif +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md pictureUpload" data-id="'+ a.id_sld +'" data-target="#uploadslid"><i class="fa fa-upload">  Unggah</i></a></center></ul></div>';
        },
      },
      {"data":"id_sld"},
      {"data":"judulsld"},
      {"data":"subjudul"},
      {"data":"prakata"},
      { 
        data:null,

        render: function ( data, type, row ) {
                    if ( data.posisi == 'col-md-push-6' && data.posisigambar == 'col-md-6 col-md-pull-6 hidden-xs' ) {
                        return 'kanan';
                    } else if (data.posisi == 'text-right-md' && data.posisigambar == 'col-md-6 hidden-xs') {
                      return 'kiri';
                    }
                    return data;
                },
                className: "dt-body-center"
        },
      {"data":"urut"},

      { 
        data:"aktif",
      render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="editor-active " disabled>';
                    }
                    return data;
                },
                className: "dt-body-center"
              }, 

        { 
        data:null,
      render: function ( data) {
                    if ( data.lokasi != null) {
                      // return '<input type="checkbox" class="editor-lokasi " disabled checked>';
                      return "<button class='btn btn-default picshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                        // return '<button type="button" class="btn btn-info btn-sm" data-judulsld="'+data.judulsld+'" data-lokasi="'+data.lokasi+'"data-toggle="modal" data-target="#pic">Lihat</button>';
                    }else{
                      // return ' <input type="file" data-id_sld="'+data.id_sld+'" name="gambar" id="gmbr" class="form-control">';
                      return '<button class="btn btn-default pictureUpload" data-id="'+data.id_sld+'" data-target="#uploadslid"><i class="fa fa-upload"</i></button>';
                      // return '<input type="checkbox" class="editor-lokasi " disabled >';
                    }
                    return data;
                },
                className: "dt-body-center"
              },     

            ],

             rowCallback: function ( row, data ) {
            // Set the checked state of the checkbox in the table
            $('input.editor-active', row).prop( 'checked', data.aktif == 1 );
            // $('input.editor-lokasi', row).prop( 'checked', data.lokasi != null  );

        },  
         "order":[[0, 'asc']],
           
    });
f.on('order.dt search.dt', function() {
  f.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();


       
});
//produk datatables
$(document).ready( function () {
  var g = $('#tproduk').DataTable({
      
      "ajax":"{{url('produkpage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
          // return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtpro btn btn-warning btn-sm" data-id_prod="'+ a.id_prod +'" data-jps="'+ a.id_jps +'"data-produk="'+ a.produk +'" data-deskripsi="'+ a.deskripsi +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-produk btn btn-danger btn-sm" data-id_prod="'+ a.id_prod +'" data-jps="'+ a.id_jps +'" data-produk="'+ a.produk +'" data-deskripsi="'+ a.deskripsi +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
          return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edtpro btn btn-md" data-id_prod="'+ a.id_prod +'" data-jps="'+ a.id_jps +'"data-produk="'+ a.produk +'"data-deskripsi="'+ a.deskripsi +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-produk btn btn-md" data-id_prod="'+ a.id_prod +'" data-jps="'+ a.id_jps +'"data-produk="'+ a.produk +'"data-deskripsi="'+ a.deskripsi +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md produkupload" data-id="'+ a.id_prod +'" data-target="#uploadpro"><i class="fa fa-upload">  Unggah Gambar</i></a> <li><a href="#" class="delete-gambar btn btn-md" data-id_gbr="'+ a.id_gbr +'" ><i class="glyphicon glyphicon-trash"> HapusGambar</i></a></li></center></ul></div>';
        
        },
      },
      {"data":"id_prod"},
      {"data":"jps"},
      {"data":"produk"},
     { 
        data:null,
      render: function ( data) {
                    // if ( data.lokasi != null) {
                    //   // return '<input type="checkbox" class="editor-lokasi " disabled checked>';
                    //   return "<button class='btn btn-default produkshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                    //     // return '<button type="button" class="btn btn-info btn-sm" data-judulsld="'+data.judulsld+'" data-lokasi="'+data.lokasi+'"data-toggle="modal" data-target="#pic">Lihat</button>';
                    // }else{
                    //   // return ' <input type="file" data-id_sld="'+data.id_sld+'" name="gambar" id="gmbr" class="form-control">';
                    //   return '<button class="btn btn-default produkupload" data-id="'+data.id_prod+'" data-target="#uploadpro"><i class="fa fa-upload"</i></button>';
                    //   // return '<input type="checkbox" class="editor-lokasi " disabled >';
                    // }
                    // return "<button class='btn btn-default produkshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                    
                    return "<button class='btn btn-default produkshow' data-toggle='modal' data-target='#produkshow-"+data.id_prod+"'><i class='fa fa-search'></i></button>";
                      
                    return data;
                },
                className: "dt-body-center"
              },   

            ],
            "order":[[0, 'asc']],
           
    });
g.on('order.dt search.dt', function() {
  g.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();
   

});
//projek datatables
$(document).ready( function () {
   var h = $('#tprojek').DataTable({
      
      "ajax":"{{url('projekpage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
           return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edtprj btn btn-md" data-id_prjt="'+ a.id_prjt +'" data-jps="'+ a.id_jps +'"data-projek="'+ a.projek +'"data-deskripsi="'+ a.deskripsi +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-projek btn btn-md" data-id_prjt="'+ a.id_prjt +'" data-jps="'+ a.id_jps +'"data-projek="'+ a.projek +'"data-deskripsi="'+ a.deskripsi +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md projekupload" data-id="'+ a.id_prjt +'" data-target="#uploadprj"><i class="fa fa-upload">  Unggah</i></a></center></ul></div>';
        
            },
      },
      {"data":"id_prjt"},
      {"data":"jps"},
      {"data":"projek"},
     { 
        data:null,
      render: function ( data) {
                    // if ( data.lokasi != null) {
                    //   // return '<input type="checkbox" class="editor-lokasi " disabled checked>';
                    //   return "<button class='btn btn-default picshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                    //     // return '<button type="button" class="btn btn-info btn-sm" data-judulsld="'+data.judulsld+'" data-lokasi="'+data.lokasi+'"data-toggle="modal" data-target="#pic">Lihat</button>';
                    // }else{
                    //   // return ' <input type="file" data-id_sld="'+data.id_sld+'" name="gambar" id="gmbr" class="form-control">';
                    //   return '<button class="btn btn-default projekupload" data-id="'+data.id_prjt+'" data-target="#uploadprj"><i class="fa fa-upload"</i></button>';
                    //   // return '<input type="checkbox" class="editor-lokasi " disabled >';
                    // }
                     return "<button class='btn btn-default projekshow' data-toggle='modal' data-target='#projekshow-"+data.id_prjt+"'><i class='fa fa-search'></i></button>";
                      
                    // return data;
                },
                className: "dt-body-center"
              },   

            ],
            
    "order":[[0, 'asc']],
           
    });
h.on('order.dt search.dt', function() {
  h.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();
});

//servis datatables
$(document).ready( function () {
  var i =  $('#tservis').DataTable({
      
      "ajax":"{{url('servispage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
          return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtsvs btn btn-warning btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-servis btn btn-danger btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
        },
      },
      {"data":"id_serv"},
      {"data":"jps"},
      {"data":"servis"},
      {"data":"ikon"},
  

            ],
            
   "order":[[0, 'asc']],
           
    });
i.on('order.dt search.dt', function() {
  i.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

});

//unduhan datatables
$(document).ready( function () {
  var j =  $('#tunduh').DataTable({
      
      "ajax":"{{url('unduhpage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
          return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtund btn btn-warning btn-sm" data-id_unduh="'+ a.id_unduh +'" data-judulunduh="'+ a.judulunduh +'"data-linkunduh="'+ a.linkunduh +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-unduh btn btn-danger btn-sm" data-id_unduh="'+ a.id_unduh +'" data-judulunduh="'+ a.judulunduh +'"data-linkunduh="'+ a.linkunduh +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
        },
      },
      {"data":"id_unduh"},
      {"data":"judulunduh"},
      {"data":"linkunduh"},
    

            ],
            
   "order":[[0, 'asc']],
           
    });
j.on('order.dt search.dt', function() {
  j.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

});
//blog datatables
$(document).ready( function () {
  var l =  $('#tabelblog').DataTable({
      
      "ajax":"{{url('blogpage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
        //   return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtsvs btn btn-warning btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-servis btn btn-danger btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
         return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edtblogs btn btn-md" data-id_blog="'+ a.id_blog +'" data-judul="'+ a.judul +'"data-rec_usr="'+ a.rec_usr +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-blog btn btn-md" data-id_blog="'+ a.id_blog +'" data-judul="'+ a.judul +'"data-rec_usr="'+ a.rec_usr +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md bloguploade" data-id="'+ a.id_blog +'" data-target="#uploadblg"><i class="fa fa-upload">  Unggah Gambar</i></a> </center></ul></div>';
        
        },
      },
      {"data":"id_blog"},
      {"data":"judul"},
      {"data":"namauser"},
       
        {
            data:null,
      render: function ( data) {
                   
                     return "<button class='btn btn-default ' data-toggle='modal' data-target='#blogshow-"+data.id_blog+"'><i class='fa fa-search'></i></button>";
                    
                    // return data;
                },
                className: "dt-body-center"
              },

            ],

            
   "order":[[0, 'asc']],
           
    });
l.on('order.dt search.dt', function() {
  l.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

});
//datatables userweb
$(document).ready( function () {
  var m =  $('#tuweb').DataTable({
      
      "ajax":"{{url('userwebpage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
        //   return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtsvs btn btn-warning btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-servis btn btn-danger btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
         return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edtusr btn btn-md" data-id_usr="'+ a.id_usr +'" data-namauser="'+ a.namauser +'"data-email="'+ a.email +'"data-jnsuser="'+ a.jnsuser +'"data-status="'+ a.status +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-userweb btn btn-md" data-id_usr="'+ a.id_usr +'" data-namauser="'+ a.namauser +'"data-email="'+ a.email +'"data-jnsuser="'+ a.jnsuser +'"data-status="'+ a.status +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md userupload" data-id="'+ a.id_usr +'" data-target="#uploadusr"><i class="fa fa-upload">  Unggah Gambar</i></a> </center></ul></div>';
        
        },
      },
      {"data":"id_usr"},
      {"data":"namauser"},
      {"data":"email"},
      {"data":"jenisuser"},
      {"data":"status",
      render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox"  class="editor-active " disabled>';
                    }
                    return data;
                },
                className: "dt-body-center"
             

      },
      { 
        data:null,
      render: function ( data) {
                    if ( data.lokasi != null) {
                      // return '<input type="checkbox" class="editor-lokasi " disabled checked>';
                      return "<button class='btn btn-default picshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                        // return '<button type="button" class="btn btn-info btn-sm" data-judulsld="'+data.judulsld+'" data-lokasi="'+data.lokasi+'"data-toggle="modal" data-target="#pic">Lihat</button>';
                    }else{
                      // return ' <input type="file" data-id_sld="'+data.id_sld+'" name="gambar" id="gmbr" class="form-control">';
                      return '<button class="btn btn-default userupload" data-id="'+data.id_usr+'" data-target="#uploadusr"><i class="fa fa-upload"</i></button>';
                      // return '<input type="checkbox" class="editor-lokasi " disabled >';
                    }
                    return data;
                },
                className: "dt-body-center"
              },   

            ],
            
            rowCallback: function ( row, data ) {
            // Set the checked state of the checkbox in the table
            $('input.editor-active', row).prop( 'checked', data.status == 1 );
            // $('input.editor-lokasi', row).prop( 'checked', data.lokasi != null  );
            },
   "order":[[0, 'asc']],
           
    });
m.on('order.dt search.dt', function() {
  m.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

});
$(document).ready( function () {
  var l =  $('#tabelklien').DataTable({
      
      "ajax":"{{url('klienpage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
        //   return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtsvs btn btn-warning btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-servis btn btn-danger btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
         return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edtkliens btn btn-md" data-id_klien="'+ a.id_klien +'" data-klien="'+ a.klien +'"data-alamat="'+ a.alamat +'"data-telp="'+ a.telp +'"data-email="'+ a.email +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-kliens btn btn-md" data-id_klien="'+ a.id_klien +'" data-klien="'+ a.klien +'"data-alamat="'+ a.alamat +'"data-telp="'+ a.telp +'"data-email="'+ a.email +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md klienupload" data-id="'+ a.id_klien +'" data-target="#uploadkln"><i class="fa fa-upload">  Unggah Gambar</i></a> </center></ul></div>';
        
        },
      },
      {"data":"id_klien"},
      {"data":"klien"},
      {"data":"alamat"},
      {"data":"telp"},
      {"data":"email"},
       
       {
            data:null,
      render: function ( data) {
                    if ( data.lokasi != null) {
                      // return '<input type="checkbox" class="editor-lokasi " disabled checked>';
                      return "<button class='btn btn-default picshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                        // return '<button type="button" class="btn btn-info btn-sm" data-judulsld="'+data.judulsld+'" data-lokasi="'+data.lokasi+'"data-toggle="modal" data-target="#pic">Lihat</button>';
                    }else{
                      // return ' <input type="file" data-id_sld="'+data.id_sld+'" name="gambar" id="gmbr" class="form-control">';
                      return '<button class="btn btn-default klienupload" data-id="'+data.id_klien+'" data-target="#uploadkln"><i class="fa fa-upload"</i></button>';
                      // return '<input type="checkbox" class="editor-lokasi " disabled >';
                    }
                    return data;
                },
                className: "dt-body-center"
              },   

            ],
            
   "order":[[0, 'asc']],
           
    });
l.on('order.dt search.dt', function() {
  l.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

});


$(document).ready( function () {
  var p =  $('#tabeltesti').DataTable({
      
      "ajax":"{{url('testipage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
        //   return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtsvs btn btn-warning btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-servis btn btn-danger btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
         return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edttesti btn btn-md" data-id_testi="'+ a.id_testi +'" data-klien="'+ a.id_klien +'"data-nama="'+ a.nama +'"data-testimoni="'+ a.testimoni +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-testi btn btn-md" data-id_testi="'+ a.id_testi +'" data-klien="'+ a.id_klien +'"data-nama="'+ a.nama +'"data-testimoni="'+ a.testimoni +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md testiupload" data-id="'+ a.id_testi +'" data-target="#uploadtst"><i class="fa fa-upload">  Unggah Gambar</i></a> </center></ul></div>';
        
        },
      },
      {"data":"id_testi"},
      {"data":"klien"},
      {"data":"nama"},
      {"data":"testimoni"},
       {
            data:null,
      render: function ( data) {
                    if ( data.lokasi != null) {
                      // return '<input type="checkbox" class="editor-lokasi " disabled checked>';
                      return "<button class='btn btn-default picshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                        // return '<button type="button" class="btn btn-info btn-sm" data-judulsld="'+data.judulsld+'" data-lokasi="'+data.lokasi+'"data-toggle="modal" data-target="#pic">Lihat</button>';
                    }else{
                      // return ' <input type="file" data-id_sld="'+data.id_sld+'" name="gambar" id="gmbr" class="form-control">';
                      return '<button class="btn btn-default testiupload" data-id="'+data.id_testi+'" data-target="#uploadtst"><i class="fa fa-upload"</i></button>';
                      // return '<input type="checkbox" class="editor-lokasi " disabled >';
                    }
                    return data;
                },
                className: "dt-body-center"
              },   
      
            ],
            
   "order":[[0, 'asc']],
           
    });
p.on('order.dt search.dt', function() {
  p.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

});


$(document).ready( function () {
  var s =  $('#tabelpartner').DataTable({
      
      "ajax":"{{url('partnerpage')}}",
      "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            },
      "columns":[
       {bSortable: false,
        data:null,
        className:"center",
        render: function(a){
          // return'<center><a href="#" class="edt btn btn-warning btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak +'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp; <a href="#" class="delete-modal btn btn-danger btn-sm" data-id_ktk="'+ a.id_ktk +'" data-jnskontak="'+ a.jnskontak+'" data-dtlkontak="'+ a.dtlkontak +'" data-ikon="'+ a.ikon +'" data-urut="'+ a.urut +'"><i class="glyphicon glyphicon-trash"></i></a></center>';
        //   return'<div class="dropdown" align="center"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><a href="#" class="edtsvs btn btn-warning btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-pencil"> Ubah</i></a>&nbsp; <a href="#" class="delete-servis btn btn-danger btn-sm" data-id_serv="'+ a.id_serv +'" data-jps="'+ a.id_jps +'"data-servis="'+ a.servis +'"data-ikon="'+ a.ikon +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></center></ul></div>';
         return'<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span></button><ul class="dropdown-menu"><center><li><a href="#" class="edtpartner btn btn-md" data-id_partner="'+ a.id_partner +'" data-partner="'+ a.partner +'"data-alamat="'+ a.alamat +'"data-telp="'+ a.telp +'"data-email="'+ a.email +'"><i class="glyphicon glyphicon-pencil">  Ubah</i></a></li><li><a href="#" class="delete-partner btn btn-md" data-id_partner="'+ a.id_partner +'" data-partner="'+ a.partner +'"data-alamat="'+ a.alamat +'"data-telp="'+ a.telp +'"data-email="'+ a.email +'"><i class="glyphicon glyphicon-trash"> Hapus</i></a></li><a href="#" class="btn btn-md partnerupload" data-id="'+ a.id_partner +'" data-target="#uploadprt"><i class="fa fa-upload">  Unggah Gambar</i></a> </center></ul></div>';
        
        },
      },
      {"data":"id_partner"},
      {"data":"partner"},
      {"data":"alamat"},
      {"data":"telp"},
      {"data":"email"},
  
         {
            data:null,
      render: function ( data) {
                    if ( data.lokasi != null) {
                      // return '<input type="checkbox" class="editor-lokasi " disabled checked>';
                      return "<button class='btn btn-default picshow' data-lokasi='/grittekno/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                        // return '<button type="button" class="btn btn-info btn-sm" data-judulsld="'+data.judulsld+'" data-lokasi="'+data.lokasi+'"data-toggle="modal" data-target="#pic">Lihat</button>';
                    }else{
                      // return ' <input type="file" data-id_sld="'+data.id_sld+'" name="gambar" id="gmbr" class="form-control">';
                      return '<button class="btn btn-default partnerupload" data-id="'+data.id_partner+'" data-target="#uploadprt"><i class="fa fa-upload"</i></button>';
                      // return '<input type="checkbox" class="editor-lokasi " disabled >';
                    }
                    return data;
                },
                className: "dt-body-center"
              },   

            ],
            
   "order":[[0, 'asc']],
           
    });
s.on('order.dt search.dt', function() {
  s.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
    cell.innerHTML = i+1;
  });
  }).draw();

});

//show slider
$(document).on('click', '.picshow', function() {
  $('#gmbr').attr('src', $(this).data('lokasi'));
  $('#picshow').modal('show');
});
// uppload slider
$(document).on('click', '.pictureUpload', function() {
  $('#picsld').val($(this).data('id'));
  $('#uploadslid').modal('show');
});
//show produk
// $(document).on('click', '.produkshow', function() {
//   $('#gbrproduk').attr('src', $(this).data('lokasi'));
//   $('#produkshow').modal('show');
// });

//show blog
$(document).on('click', '.blogshow', function() {
  $('#gbrshowblog').attr('src', $(this).data('lokasi'));
  $('#blogshow').modal('show');
});

//show blog
$(document).on('click', '.projekshow', function() {
  $('#gbrshowprojek').attr('src', $(this).data('lokasi'));
  $('#projekshow').modal('show');
});
// upload produk
$(document).on('click', '.produkupload', function() {
  $('#picprd').val($(this).data('id'));
  $('#uploadpro').modal('show');
});

// upload projek
$(document).on('click', '.projekupload', function() {
  $('#picprj').val($(this).data('id'));
  $('#uploadprj').modal('show');
});

// // upload userweb
$(document).on('click', '.userupload', function() {
  $('#picusr').val($(this).data('id'));
  $('#uploadusr').modal('show');
});

// // upload klien
$(document).on('click', '.klienupload', function() {
  $('#pickln').val($(this).data('id'));
  $('#uploadkln').modal('show');
});

// // upload testi
$(document).on('click', '.testiupload', function() {
  $('#pictst').val($(this).data('id'));
  $('#uploadtst').modal('show');
});

// // upload partner
$(document).on('click', '.partnerupload', function() {
  $('#picprt').val($(this).data('id'));
  $('#uploadprt').modal('show');
});

// // upload blog
$(document).on('click', '.bloguploade', function() {
  $('#picblg').val($(this).data('id'));
  $('#uploadblg').modal('show');
});



// function Edit profil
$(document).on('click', '.edit-modal', function() {
$('#footer_action_profil').text(" Simpan");
 $('#footer_action_profil').addClass('glyphicon-check');
$('#footer_action_profil').removeClass('glyphicon-trash');
$('.actionBtn').addClass('btn-success');
$('.actionBtn').removeClass('btn-danger');
$('.actionBtn').addClass('ubahprofil');
$('.modal-title').text('Edit Profil');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#rid').val($('#idp').data('id'));
$('#slogan').val($('#idp').data('slogan'));
$('#highlight').val($('#idp').data('highlight'));
$('#profil').val($('#idp').data('profil'));
$('#edt').modal('show');
});
$('.modal-footer').on('click', '.ubahprofil', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'patch',
    url: 'profile',
    data: {
'id_prof': $("#rid").val(),
'slogan': $('#slogan').val(),
'highlight': $('#highlight').val(),
'profil': $('#profil').val()
    },
    success: function(data) {



        console.log(data);
          $('#sl').replaceWith(data.slogan);
          $('#hi').replaceWith(data.highlight);
          $('#pr').replaceWith(data.profil);
        
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#editpr').trigger('reset');
        $('#edt').modal('hide');
                       
        

    }
  
  });
});

{{-- ajax Form Add kontak--}}
  $(document).on('click','.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Kontak');
  });
  $("#add").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addPost',
      data: {
        'jnskontak': $('input[name=jnskontak]').val(),
        'dtlkontak': $('input[name=dtlkontak]').val(),
        'ikon': $('input[name=ikon]').val(),
        'urut': $('input[name=urut]').val(),


      },
      success: function(data){

          $('.errorjns').addClass('hidden');
          $('.errorkon').addClass('hidden');
          $('.errorIkon').addClass('hidden');
          $('.errorurut').addClass('hidden');



        if ((data.errors)) {
                setTimeout(function () {
                  $('#create').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                 if (data.errors.jnskontak) {
                            $('.errorjns').removeClass('hidden');
                            $('.errorjns').text(data.errors.jnskontak);
                        }
                        if (data.errors.dtlkontak) {
                            $('.errorkon').removeClass('hidden');
                            $('.errorkon').text(data.errors.dtlkontak);
                        } 
                         if (data.errors.ikon) {
                            $('.errorIkon').removeClass('hidden');
                            $('.errorIkon').text(data.errors.ikon);
                        }  if (data.errors.urut) {
                            $('.errorurut').removeClass('hidden');
                            $('.errorurut').text(data.errors.urut);
                        } 
                       
                        
        } else {
        var table = $('#knt').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#formu').trigger('reset');
        $('#create').modal('hide');
                       
         }
      }
    });
    $('#jenis').val('');
    $('#kontak').val('');
    $('#icon').val('');
    $('#urutt').val('');
  });

// function Edit kontak
$(document).on('click', '.edt', function() {
$('#footer_action_kontak').text(" Simpan");
$('#footer_action_kontak').addClass('glyphicon-check');
$('#footer_action_kontak').removeClass('glyphicon-trash');
$('.actionBtnko').addClass('btn-success');
$('.actionBtnko').removeClass('btn-danger');
$('.actionBtnko').addClass('ubahkontak');
$('.modal-title').text('Ubah Kontak');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#fid').val($(this).data('id_ktk'));
$('#jns').val($(this).data('jnskontak'));
$('#kntk').val($(this).data('dtlkontak'));
$('#ikn').val($(this).data('ikon'));
$('#urt').val($(this).data('urut'));
$('#edtknk').modal('show');
});

$('.modal-footer').on('click', '.ubahkontak', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'editkon',
    data: {
'id_ktk': $("#fid").val(),
'jnskontak': $('#jns').val(),
'dtlkontak': $('#kntk').val(),
'ikon': $('#ikn').val(),
'urut': $('#urt').val()
    },
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#knt').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubah').trigger('reset');
        $('#edtknk').modal('hide');
                       
        

    }
  
  });
});
// form Delete kontak
$(document).on('click', '.delete-modal', function() {
$('#footer_action_hapusknt').text(" Hapus");
$('#footer_action_hapusknt').removeClass('glyphicon-check');
$('#footer_action_hapusknt').addClass('glyphicon-trash');
$('.actionBtnk').removeClass('btn-success');
$('.actionBtnk').addClass('btn-danger');
$('.actionBtnk').addClass('deletekontak');
$('.modal-title').text('Hapus Kontak');
$('.id_ktk').text($(this).data('id_ktk'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.jenis').html($(this).data('name'));
$('#hpsknt').modal('show');
});

$('.modal-footer').on('click', '.deletekontak', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delkon',
    data: {
      'id_ktk': $('.id_ktk').text()
    },
    success: function(data){

    
       var table = $('#knt').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsknt').modal('hide');
                
    }
  });


});

{{-- ajax Form Add Sosmed--}}
  $(document).on('click','.create-sosmed', function() {
    $('#buatsosmed').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Sosmed');
  });
  $("#tambahsos").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addsosmed',
      data: {
        'jnssosmed': $('input[name=jnssosmed]').val(),
        'dtlsosmed': $('input[name=dtlsosmed]').val(),
        'ikonsosmed': $('input[name=ikonsosmed]').val(),
        'urutsosmed': $('input[name=urutsosmed]').val(),


      },
      success: function(data){

          $('.errorjns').addClass('hidden');
          $('.errorsos').addClass('hidden');
          $('.errorIkon').addClass('hidden');
          $('.errorurut').addClass('hidden');



        if ((data.errors)) {
                setTimeout(function () {
                  $('#buatsosmed').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                 if (data.errors.jnssosmed) {
                            $('.errorjns').removeClass('hidden');
                            $('.errorjns').text(data.errors.jnssosmed);
                        }
                        if (data.errors.dtlsosmed) {
                            $('.errorsos').removeClass('hidden');
                            $('.errorsos').text(data.errors.dtlsosmed);
                        } 
                         if (data.errors.ikonsosmed) {
                            $('.errorIkon').removeClass('hidden');
                            $('.errorIkon').text(data.errors.ikonsosmed);
                        }  if (data.errors.urutsosmed) {
                            $('.errorurut').removeClass('hidden');
                            $('.errorurut').text(data.errors.urutsosmed);
                        } 
                       
                        
        } else {
        var table = $('#sos').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ssd').trigger('reset');
        $('#buatsosmed').modal('hide');
                       
         }
      }
    });
    $('#jenissosmed').val('');
    $('#sosmed').val('');
    $('#iconsosmed').val('');
    $('#urutsosmed').val('');
  });
//function Edit Sosmed
$(document).on('click', '.editsos', function() {
$('#footer_action_sosmed').text(" Simpan");
$('#footer_action_sosmed').addClass('glyphicon-check');
$('#footer_action_sosmed').removeClass('glyphicon-trash');
$('.actionBtnsos').addClass('btn-success');
$('.actionBtnsos').removeClass('btn-danger');
$('.actionBtnsos').addClass('ubah');
$('.modal-title').text('Ubah Sosmed');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#sid').val($(this).data('id_sosmed'));
$('#jnssmd').val($(this).data('jnssosmed'));
$('#media').val($(this).data('dtlsosmed'));
$('#iknsmd').val($(this).data('ikon'));
$('#urtsmd').val($(this).data('urut'));
$('#edtsm').modal('show');
});

$('.modal-footer').on('click', '.ubah', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'editsosmed',
    data: {
'id_sosmed': $("#sid").val(),
'jnssosmed': $('#jnssmd').val(),
'dtlsosmed': $('#media').val(),
'ikonsosmed': $('#iknsmd').val(),
'urutsosmed': $('#urtsmd').val()
    },
    success: function(data) {
       
   //   console.log(data);
        
        var table = $('#sos').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahsos').trigger('reset');
        $('#edtsm').modal('hide');
                       
         

    }
  
  });
});
// form Delete sosmed
$(document).on('click', '.delete-sos', function() {
$('#footer_action_hapusmd').text(" Hapus");
$('#footer_action_hapusmd').removeClass('glyphicon-check');
$('#footer_action_hapusmd').addClass('glyphicon-trash');
$('.actionBtns').removeClass('btn-success');
$('.actionBtns').addClass('btn-danger');
$('.actionBtns').addClass('hapussosmed');
$('.modal-title').text('Hapus Sosmed');
$('.id_sosmed').text($(this).data('id_sosmed'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.jenis').html($(this).data('name'));
$('#hpsmd').modal('show');
});

$('.modal-footer').on('click', '.hapussosmed', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delsos',
    data: {
      'id_sosmed': $('.id_sosmed').text()
    },
    success: function(data){

    
       var table = $('#sos').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsmd').modal('hide');
                
    }
  });


});

{{-- ajax Form Add Office--}}
  $(document).on('click','.create-kantor', function() {
    $('#offc').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Office');
  });
  $("#tambahkantor").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   
    var type = "POST";
    var url ="tambahalamat";
    var form = {
      nama: $('#nama').val(),
      alamat: $('#address').val(),
      kota: $('#city').val(),
      kodepos: $('#kp').val(),
      koordinat: $('#kdnt').val(),
      telp: $('#telph').val(),
      aktif: $('#aktf:checked').val(),
    }
    
    console.log(form);
    $.ajax({
      type: 'POST',
      url: url,
      data : form,
    // $.ajax({
    //   type: 'POST',
    //   url: 'tambahalamat',
    //   data: {
    //     'nama': $('input[name=nama]').val(),
    //     'alamat': $('input[name=alamat]').val(),
    //     'kota': $('input[name=kota]').val(),
    //     'kodepos': $('input[name=kodepos]').val(),
    //     'koordinat': $('input[name=koordinat]').val(),
    //     'telp': $('input[name=telp]').val(),
    //     'aktif': $('input[name=aktif]').val(),


    //   },
      success: function(data){

          $('.errorjns').addClass('hidden');
          $('.errorkon').addClass('hidden');
          $('.errorIkon').addClass('hidden');
          $('.errorurut').addClass('hidden');



        if ((data.errors)) {
                setTimeout(function () {
                  $('#offc').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                 if (data.errors.jnskontak) {
                            $('.errorjns').removeClass('hidden');
                            $('.errorjns').text(data.errors.jnskontak);
                        }
                        if (data.errors.dtlkontak) {
                            $('.errorkon').removeClass('hidden');
                            $('.errorkon').text(data.errors.dtlkontak);
                        } 
                         if (data.errors.ikon) {
                            $('.errorIkon').removeClass('hidden');
                            $('.errorIkon').text(data.errors.ikon);
                        }  if (data.errors.urut) {
                            $('.errorurut').removeClass('hidden');
                            $('.errorurut').text(data.errors.urut);
                        } 
                       
                        
        } else {
        var table = $('#office').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#kantor').trigger('reset');
        $('#offc').modal('hide');
                       
         }
      }
    });
    $('#nama').val('');
    $('#address').val('');
    $('#city').val('');
    $('#kp').val('');
    $('#kdnt').val('');
    $('#telph').val('');
    $('#aktf').val('');
  });

// function Edit office
$(document).on('click', '.edit-off', function() {
$('#footer_action_office').text(" Simpan");
$('#footer_action_office').addClass('glyphicon-check');
$('#footer_action_office').removeClass('glyphicon-trash');
$('.actionBtnn').addClass('btn-success');
$('.actionBtnn').removeClass('btn-danger');
$('.actionBtnn').addClass('ubahoffice');
$('.modal-title').text('Ubah Office');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#oid').val($(this).data('id_off'));
$('#nm').val($(this).data('nama'));
$('#almt').val($(this).data('alamat'));
$('#kt').val($(this).data('kota'));
$('#kodpos').val($(this).data('kodepos'));
$('#krdt').val($(this).data('koordinat'));
$('#phone').val($(this).data('telp'));
$('#akf').val($(this).data('aktif'));
$('#edtoff').modal('show');
});

$('.modal-footer').on('click', '.ubahoffice', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   var type = "POST";
    var url ="ubahalamat";
    var formof = {
      id_off: $('#oid').val(),
      nama: $('#nm').val(),
      alamat: $('#almt').val(),
      kota: $('#kt').val(),
      kodepos: $('#kodpos').val(),
      koordinat: $('#krdt').val(),
      telp: $('#phone').val(),
      aktif: $('#akfedit:checked').val(),
    }
    
    console.log(formof);
    $.ajax({
      type: type,
      url: url,
      data : formof,
//   $.ajax({
//     type: 'POST',
//     url: 'ubahalamat',
//     data: {
// 'id_off': $("#oid").val(),
// 'nama': $('#nm').val(),
// 'alamat': $('#almt').val(),
// 'kota': $('#kt').val(),
// 'kodepos': $('#kodpos').val(),
// 'koordinat': $('#krdt').val(),
// 'telp': $('#phone').val(),
// 'aktif': $('#akf').val()

//     },
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#office').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahofc').trigger('reset');
        $('#edtoff').modal('hide');
                       
        

    }
  
  });
});

// form Delete office
$(document).on('click', '.delete-alm', function() {
$('#footer_action_hapusoff').text(" Hapus");
$('#footer_action_hapusoff').removeClass('glyphicon-check');
$('#footer_action_hapusoff').addClass('glyphicon-trash');
$('.actionBtno').removeClass('btn-success');
$('.actionBtno').addClass('btn-danger');
$('.actionBtno').addClass('hapusoffice');
$('.modal-title').text('Hapus Office');
$('.id_off').text($(this).data('id_off'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.nama').html($(this).data('name'));
$('#hpsof').modal('show');
});

$('.modal-footer').on('click', '.hapusoffice', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'hapusalamat',
    data: {
      'id_off': $('.id_off').text()
    },
    success: function(data){

    
       var table = $('#office').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsof').modal('hide');
                
    }
  });
});

{{-- ajax Form Add Jenis--}}
  $(document).on('click','.create-jenis', function() {
    $('#mjenis').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Jenis');
  });
  $("#tambahjenis").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addjenis',
      data: {
        'jps': $('input[name=jps]').val(),
      },
      success: function(data){

          $('.errorjps').addClass('hidden');
         
        if ((data.errors)) {
                setTimeout(function () {
                  $('#mjenis').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                 if (data.errors.jps) {
                            $('.errorjps').removeClass('hidden');
                            $('.errorjps').text(data.errors.jps);
                        }
                       
                       
                        
        } else {
        var table = $('#datajps').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#baru').trigger('reset');
        $('#mjenis').modal('hide');
                       
         }
      }
    });
    $('#jenis').val('');
   
  });

// function Edit Jenis
$(document).on('click', '.edtjenis', function() {
$('#footer_action_jenis').text(" Simpan");
$('#footer_action_jenis').addClass('glyphicon-check');
$('#footer_action_jenis').removeClass('glyphicon-trash');
$('.actionBtnj').addClass('btn-success');
$('.actionBtnj').removeClass('btn-danger');
$('.actionBtnj').addClass('ubahjenis');
$('.modal-title').text('Ubah Jenis');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#jid').val($(this).data('id_jps'));
$('#jnsp').val($(this).data('jps'));

$('#edtjns').modal('show');
});

$('.modal-footer').on('click', '.ubahjenis', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'editjenis',
    data: {
'id_jps': $("#jid").val(),
'jps': $('#jnsp').val()
    },
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#datajps').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahjps').trigger('reset');
        $('#edtjns').modal('hide');
                       
        

    }
  
  });
});
// form Delete jenis
$(document).on('click', '.delete-jenis', function() {
$('#footer_action_hpsjns').text(" Hapus");
$('#footer_action_hpsjns').removeClass('glyphicon-check');
$('#footer_action_hpsjns').addClass('glyphicon-trash');
$('.actionBtnhj').removeClass('btn-success');
$('.actionBtnhj').addClass('btn-danger');
$('.actionBtnhj').addClass('deletejenis');
$('.modal-title').text('Hapus Jenis');
$('.id_jps').text($(this).data('id_jps'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.jenis').html($(this).data('name'));
$('#hpsjn').modal('show');
});

$('.modal-footer').on('click', '.deletejenis', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'deljns',
    data: {
      'id_jps': $('.id_jps').text()
    },
    success: function(data){

    
       var table = $('#datajps').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsjn').modal('hide');
                
    }
  });


});
{{-- ajax Form Add Linkvideo--}}
  $(document).on('click','.create-video', function() {
    $('#mvid').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Jenis');
  });
  $("#tambahvid").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addvideo',
      data: {
        'judulvid': $('input[name=judulvid]').val(),
        'linkvid': $('input[name=linkvid]').val(),
      },
      success: function(data){

          $('.errorjdl').addClass('hidden');
          $('.errorlnk').addClass('hidden');
         
        if ((data.errors)) {
                setTimeout(function () {
                  $('#mvid').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                 if (data.errors.judulvid) {
                            $('.errorjdl').removeClass('hidden');
                            $('.errorjdl').text(data.errors.judulvid);
                        }
                         if (data.errors.linkvid) {
                            $('.errorlnk').removeClass('hidden');
                            $('.errorlnk').text(data.errors.linkvid);
                        }
                       
                       
                        
        } else {
        var table = $('#linkvd').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#linkbaru').trigger('reset');
        $('#mvid').modal('hide');
                       
         }
      }
    });
    $('#jdl').val('');
    $('#lnk').val('');
   
  });
// function Edit Video
$(document).on('click', '.edtlink', function() {
$('#footer_action_video').text(" Simpan");
$('#footer_action_video').addClass('glyphicon-check');
$('#footer_action_video').removeClass('glyphicon-trash');
$('.actionBtnv').addClass('btn-success');
$('.actionBtnv').removeClass('btn-danger');
$('.actionBtnv').addClass('ubahvideo');
$('.modal-title').text('Ubah link Video');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#jdid').val($(this).data('id_vid'));
$('#jdlv').val($(this).data('judulvid'));
$('#lnkv').val($(this).data('linkvid'));

$('#edtvd').modal('show');
});

$('.modal-footer').on('click', '.ubahvideo', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'video',
    data: {
'id_vid': $("#jdid").val(),
'judulvid': $('#jdlv').val(),
'linkvid': $('#lnkv').val()
    },
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#linkvd').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahvd').trigger('reset');
        $('#edtvd').modal('hide');
                       
        

    }
  
  });
});
// form Delete video
$(document).on('click', '.delete-link', function() {
$('#footer_action_hapus').text(" Hapus");
$('#footer_action_hapus').removeClass('glyphicon-check');
$('#footer_action_hapus').addClass('glyphicon-trash');
$('.actionBtnvd').removeClass('btn-success');
$('.actionBtnvd').addClass('btn-danger');
$('.actionBtnvd').addClass('hapusvideo');
$('.modal-title').text('Hapus Video');
$('.id_vid').text($(this).data('id_vid'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.judulvid').html($(this).data('name'));
$('#hpsvd').modal('show');
});

$('.modal-footer').on('click', '.hapusvideo', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delvid',
    data: {
      'id_vid': $('.id_vid').text()
    },
    success: function(data){

    
       var table = $('#linkvd').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsvd').modal('hide');
                
    }
  });
});
{{-- ajax Form Add Slider--}}
  $(document).on('click','.create-slider', function() {
    $('#mslider').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Slider');
  });
  $("#tambahslider").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    
    var posisisld = $("input:radio[name=posisisld]:checked").val();
    var [posisigambar, posisiteks]=posisisld.split(":");
    var type = "POST";
    var url ="addslid";
    var formData = {
      judulsld: $('#jdlsld').val(),
      subjudul: $('#sub').val(),
      prakata: $('#pra').val(),
      deskripsi: $('#dsk').val(),
      posisigambar: posisigambar,
      posisiteks: posisiteks,
      urut: $('#urutan').val(),
      aktif: $('#aktipsld:checked').val(),
    }
    
    console.log(formData);
    $.ajax({
      type: 'POST',
      url: url,
      data : formData,
      
  
      success: function(data){

         
        
        if ((data.errors)) {
                setTimeout(function () {
                  $('#mslider').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                          
                        
        } else {
        var table = $('#slid').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#sliderbaru').trigger('reset');
        $('#mslider').modal('hide');
                       
         }
      }
    });
    // $('#jdlsld').val('');
    // $('#sub').val('');
    // $('#pra').val('');
    // $('#pos').val('');
    // $('#urutan').val('');
    // $('#aktip').val('');
   
  });
// function Edit slider
$(document).on('click', '.edtsl', function() {
$('#footer_action_slider').text(" Simpan");
$('#footer_action_slider').addClass('glyphicon-check');
$('#footer_action_slider').removeClass('glyphicon-trash');
$('.actionBtnl').addClass('btn-success');
$('.actionBtnl').removeClass('btn-danger');
$('.actionBtnl').addClass('ubahslider');
$('.modal-title').text('Ubah Slider');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#sld_id').val($(this).data('id_sld'));
$('#jsld').val($(this).data('judulsld'));
$('#subjdl').val($(this).data('subjudul'));
$('#prkt').val($(this).data('prakata'));
$('#des').val($(this).data('deskripsi'));

// $('#dsk').val($(this).data('deskripsi'));
 // posisigambar: posisigambar;
 // posisiteks: posisiteks;
$('#ur').val($(this).data('urut'));
$('#edtsli').modal('show');
});

$('.modal-footer').on('click', '.ubahslider', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })

  var posisisld = $("input:radio[name=posisiedit]:checked").val();
  var [posisigambar, posisiteks]=posisisld.split(":");
   var type = "PATCH";
   var id_sld =$('#sld_id').val();
    var url ="sliderubah"+"/"+id_sld;
    var Data = {
      id: id_sld,
      judulsld: $('#jsld').val(),
      subjudul: $('#subjdl').val(),
      prakata: $('#prkt').val(),
      deskripsi: $('#des').val(),
      posisigambar: posisigambar,
      posisiteks: posisiteks,
      urut: $('#ur').val(),
      aktif: $('#aktipedit:checked').val(),
    }
    
    console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,

    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#slid').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahsl').trigger('reset');
        $('#edtsli').modal('hide');
                       
        

    }
  
  });
});
// form Delete slider
$(document).on('click', '.delete-sl', function() {
$('#footer_action_hpsslid').text(" Hapus");
$('#footer_action_hpsslid').removeClass('glyphicon-check');
$('#footer_action_hpsslid').addClass('glyphicon-trash');
$('.actionBtnd').removeClass('btn-success');
$('.actionBtnd').addClass('btn-danger');
$('.actionBtnd').addClass('hapusslider');
$('.modal-title').text('Hapus Slider');
$('.id_sld').text($(this).data('id_sld'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.judulsld').html($(this).data('name'));
$('#hpssl').modal('show');
});

$('.modal-footer').on('click', '.hapusslider', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delslid',
    data: {
      'id_sld': $('.id_sld').text()
    },
    success: function(data){

    
       var table = $('#slid').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpssl').modal('hide');
                
    }
  });
});
{{-- ajax Form Add produk--}}
  $(document).on('click','.create-produk', function() {
    $('#mprod').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Produk');
  });
  $("#tambahpro").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   
    var type ="POST";
     var url ="addprd";
    var formData = {
      jps: $('#jpsr').val(),
      produk: $('#pdk').val(),
      deskripsi: $('#desk').val(),
     
    }
    
    console.log(formData);
    $.ajax({
      type: type,
      url: url,
      data : formData,
      
      success: function(data){

          // $('.errorjns').addClass('hidden');
          // $('.errorkon').addClass('hidden');
          // $('.errorIkon').addClass('hidden');
          // $('.errorurut').addClass('hidden');



        // if ((data.errors)) {
        //         setTimeout(function () {
        //           $('#create').modal('show')
        //           toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
        //            }, 500);  
        //          if (data.errors.jnskontak) {
        //                     $('.errorjns').removeClass('hidden');
        //                     $('.errorjns').text(data.errors.jnskontak);
        //                 }
        //                 if (data.errors.dtlkontak) {
        //                     $('.errorkon').removeClass('hidden');
        //                     $('.errorkon').text(data.errors.dtlkontak);
        //                 } 
        //                  if (data.errors.ikon) {
        //                     $('.errorIkon').removeClass('hidden');
        //                     $('.errorIkon').text(data.errors.ikon);
        //                 }  if (data.errors.urut) {
        //                     $('.errorurut').removeClass('hidden');
        //                     $('.errorurut').text(data.errors.urut);
        //                 } 
                       
                        
        // } else {
        var table = $('#tproduk').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#prdkbaru').trigger('reset');
        $('#mprod').modal('hide');
                       
         // }
      }
    });
    
    
  });
// function Edit produk
$(document).on('click', '.edtpro', function() {
$('#footer_action_produk').text(" Simpan");
$('#footer_action_produk').addClass('glyphicon-check');
$('#footer_action_produk').removeClass('glyphicon-trash');
$('.actionBtng').addClass('btn-success');
$('.actionBtng').removeClass('btn-danger');
$('.actionBtng').addClass('ubahproduk');
$('.modal-title').text('Ubah Slider');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#pro_id').val($(this).data('id_prod'));
$('#jspv').val($(this).data('jps'));
$('#prk').val($(this).data('produk'));
$('#dk').val($(this).data('deskripsi'));

$('#modalpro').modal('show');
});

$('.modal-footer').on('click', '.ubahproduk', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })

 
   var type = "PATCH";
   var id_prod =$('#pro_id').val();
    var url ="editprd"+"/"+id_prod;
    var Data = {
      id_prod: id_prod,
      jps: $('#jspv').val(),
      produk: $('#prk').val(),
      deskripsi: $('#dk').val(),
     
    }
    
    console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,

    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tproduk').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#produkedit').trigger('reset');
        $('#modalpro').modal('hide');
                       
        

    }
  
  });
});
// form Delete produk
$(document).on('click', '.delete-produk', function() {
$('#footer_action_hpsprod').text(" Hapus");
$('#footer_action_hpsprod').removeClass('glyphicon-check');
$('#footer_action_hpsprod').addClass('glyphicon-trash');
$('.actionBtn_hapusproduk').removeClass('btn-success');
$('.actionBtn_hapusproduk').addClass('btn-danger');
$('.actionBtn_hapusproduk').addClass('hapusproduk');
$('.modal-title').text('Hapus Produk');
$('.id_prod').text($(this).data('id_prod'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.jps').html($(this).data('name'));
$('#produkhps').modal('show');
});

$('.modal-footer').on('click', '.hapusproduk', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delprd',
    data: {
      'id_prod': $('.id_prod').text()
    },
    success: function(data){

    
       var table = $('#tproduk').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#produkhps').modal('hide');
                
    }
  });
});
{{-- ajax Form Add Projek--}}
  $(document).on('click','.create-projek', function() {
    $('#mproj').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Projek');
  });
  $("#tambahprj").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   
    var type ="POST";
     var url ="addprj";
    var formData = {
      jps: $('#jpsl').val(),
      projek: $('#pjk').val(),
      deskripsi: $('#deskr').val(),
     
    }
    
    console.log(formData);
    $.ajax({
      type: type,
      url: url,
      data : formData,
      
      success: function(data){

          // $('.errorjns').addClass('hidden');
          // $('.errorkon').addClass('hidden');
          // $('.errorIkon').addClass('hidden');
          // $('.errorurut').addClass('hidden');



        // if ((data.errors)) {
        //         setTimeout(function () {
        //           $('#create').modal('show')
        //           toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
        //            }, 500);  
        //          if (data.errors.jnskontak) {
        //                     $('.errorjns').removeClass('hidden');
        //                     $('.errorjns').text(data.errors.jnskontak);
        //                 }
        //                 if (data.errors.dtlkontak) {
        //                     $('.errorkon').removeClass('hidden');
        //                     $('.errorkon').text(data.errors.dtlkontak);
        //                 } 
        //                  if (data.errors.ikon) {
        //                     $('.errorIkon').removeClass('hidden');
        //                     $('.errorIkon').text(data.errors.ikon);
        //                 }  if (data.errors.urut) {
        //                     $('.errorurut').removeClass('hidden');
        //                     $('.errorurut').text(data.errors.urut);
        //                 } 
                       
                        
        // } else {
        var table = $('#tprojek').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#prjkbaru').trigger('reset');
        $('#mproj').modal('hide');
                       
         // }
      }
    });
    
    
  });
// function Edit projek
$(document).on('click', '.edtprj', function() {
$('#footer_action_projek').text(" Simpan");
$('#footer_action_projek').addClass('glyphicon-check');
$('#footer_action_projek').removeClass('glyphicon-trash');
$('.actionBtnb').addClass('btn-success');
$('.actionBtnb').removeClass('btn-danger');
$('.actionBtnb').addClass('ubahprojek');
$('.modal-title').text('Ubah Projek');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#prj_id').val($(this).data('id_prjt'));
$('#jsps').val($(this).data('jps'));
$('#prjk').val($(this).data('projek'));
$('#dr').val($(this).data('deskripsi'));

$('#modalprj').modal('show');
});

$('.modal-footer').on('click', '.ubahprojek', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })

 
   var type = "PATCH";
   var id_prjt =$('#prj_id').val();
    var url ="editprjk"+"/"+id_prjt;
    var Data = {
      id_prjt: id_prjt,
      jps: $('#jsps').val(),
      projek: $('#prjk').val(),
      deskripsi: $('#dr').val(),
     
    }
    
    console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,

    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tprojek').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#projekedit').trigger('reset');
        $('#modalprj').modal('hide');
                       
        

    }
  
  });
});
// form Delete projek
$(document).on('click', '.delete-projek', function() {
$('#footer_action_hpsprjt').text(" Hapus");
$('#footer_action_hpsprjt').removeClass('glyphicon-check');
$('#footer_action_hpsprjt').addClass('glyphicon-trash');
$('.actionBtn_hapusprojek').removeClass('btn-success');
$('.actionBtn_hapusprojek').addClass('btn-danger');
$('.actionBtn_hapusprojek').addClass('hapusprojek');
$('.modal-title').text('Hapus Projek');
$('.id_prjt').text($(this).data('id_prjt'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.jps').html($(this).data('name'));
$('#projekhps').modal('show');
});

$('.modal-footer').on('click', '.hapusprojek', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delprjk',
    data: {
      'id_prjt': $('.id_prjt').text()
    },
    success: function(data){

    
       var table = $('#tprojek').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#projekhps').modal('hide');
                
    }
  });
});
{{-- ajax Form Add Servis--}}
  $(document).on('click','.create-servis', function() {
    $('#mser').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Servis');
  });
  $("#tambahsrs").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   
    var type ="POST";
     var url ="addservis";
    var formData = {
      jps: $('#jpss').val(),
      servis: $('#srs').val(),
      ikon: $('#in').val(),
     
    }
    
    console.log(formData);
    $.ajax({
      type: type,
      url: url,
      data : formData,
      
      success: function(data){

          // $('.errorjns').addClass('hidden');
          // $('.errorkon').addClass('hidden');
          // $('.errorIkon').addClass('hidden');
          // $('.errorurut').addClass('hidden');



        // if ((data.errors)) {
        //         setTimeout(function () {
        //           $('#create').modal('show')
        //           toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
        //            }, 500);  
        //          if (data.errors.jnskontak) {
        //                     $('.errorjns').removeClass('hidden');
        //                     $('.errorjns').text(data.errors.jnskontak);
        //                 }
        //                 if (data.errors.dtlkontak) {
        //                     $('.errorkon').removeClass('hidden');
        //                     $('.errorkon').text(data.errors.dtlkontak);
        //                 } 
        //                  if (data.errors.ikon) {
        //                     $('.errorIkon').removeClass('hidden');
        //                     $('.errorIkon').text(data.errors.ikon);
        //                 }  if (data.errors.urut) {
        //                     $('.errorurut').removeClass('hidden');
        //                     $('.errorurut').text(data.errors.urut);
        //                 } 
                       
                        
        // } else {
        var table = $('#tservis').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#servbaru').trigger('reset');
        $('#mser').modal('hide');
                       
         // }
      }
    });
    
    
  });
// function Edit servis
$(document).on('click', '.edtsvs', function() {
$('#footer_action_servis').text(" Simpan");
$('#footer_action_servis').addClass('glyphicon-check');
$('#footer_action_servis').removeClass('glyphicon-trash');
$('.actionBtnr').addClass('btn-success');
$('.actionBtnr').removeClass('btn-danger');
$('.actionBtnr').addClass('ubahservis');
$('.modal-title').text('Ubah Servis');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#serv_id').val($(this).data('id_serv'));
$('#jsk').val($(this).data('jps'));
$('#srv').val($(this).data('servis'));
$('#io').val($(this).data('ikon'));

$('#modalsrv').modal('show');
});

$('.modal-footer').on('click', '.ubahservis', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })

 
   var type = "PATCH";
   var id_serv =$('#serv_id').val();
    var url ="editserv"+"/"+id_serv;
    var Data = {
      id_serv: id_serv,
      jps: $('#jsk').val(),
      servis: $('#srv').val(),
      ikon: $('#io').val(),
     
    }
    
    console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,

    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tservis').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#servisedit').trigger('reset');
        $('#modalsrv').modal('hide');
                       
        

    }
  
  });
});
// form Delete servis
$(document).on('click', '.delete-servis', function() {
$('#footer_action_hpsserv').text(" Hapus");
$('#footer_action_hpsserv').removeClass('glyphicon-check');
$('#footer_action_hpsserv').addClass('glyphicon-trash');
$('.actionBtn_hapusservis').removeClass('btn-success');
$('.actionBtn_hapusservis').addClass('btn-danger');
$('.actionBtn_hapusservis').addClass('hapusservis');
$('.modal-title').text('Hapus Servis');
$('.id_serv').text($(this).data('id_serv'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.jps').html($(this).data('name'));
$('#servishps').modal('show');
});

$('.modal-footer').on('click', '.hapusservis', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delserv',
    data: {
      'id_serv': $('.id_serv').text()
    },
    success: function(data){

    
       var table = $('#tservis').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#servishps').modal('hide');
                
    }
  });
});

{{-- ajax Form Add unduhan--}}
  $(document).on('click','.create-unduhan', function() {
    $('#munduh').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Unduhan');
  });
  $("#tambahunduh").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addunduh',
      data: {
        'judulunduh': $('input[name=judulunduh]').val(),
        'linkunduh': $('input[name=linkunduh]').val(),
      },
      success: function(data){

          $('.errorjdln').addClass('hidden');
          $('.errorlnknd').addClass('hidden');
         
        if ((data.errors)) {
                setTimeout(function () {
                  $('#munduh').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                 if (data.errors.judulunduh) {
                            $('.errorjdln').removeClass('hidden');
                            $('.errorjdln').text(data.errors.judulunduh);
                        }
                         if (data.errors.linkunduh) {
                            $('.errorlnknd').removeClass('hidden');
                            $('.errorlnknd').text(data.errors.linkunduh);
                        }
                       
                       
                        
        } else {
        var table = $('#tunduh').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#unduhbaru').trigger('reset');
        $('#munduh').modal('hide');
                       
         }
      }
    });
    $('#jdlundh').val('');
    $('#ud').val('');
   
  });
// function Edit Unduhan
$(document).on('click', '.edtund', function() {
$('#footer_action_unduh').text(" Simpan");
$('#footer_action_unduh').addClass('glyphicon-check');
$('#footer_action_unduh').removeClass('glyphicon-trash');
$('.actionBtnq').addClass('btn-success');
$('.actionBtnq').removeClass('btn-danger');
$('.actionBtnq').addClass('ubahunduh');
$('.modal-title').text('Ubah Unduhan');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#jdun').val($(this).data('id_unduh'));
$('#jdlu').val($(this).data('judulunduh'));
$('#lnku').val($(this).data('linkunduh'));

$('#edtunduh').modal('show');
});

$('.modal-footer').on('click', '.ubahunduh', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  var type = "PATCH";
   var id_unduh =$('#jdun').val();
    var url ="ubahunduhan"+"/"+id_unduh;
    var Data = {
      id_unduh: id_unduh,
      judulunduh: $('#jdlu').val(),
      linkunduh: $('#lnku').val(),
     }
     console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tunduh').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahudh').trigger('reset');
        $('#edtunduh').modal('hide');
                       
        

    }
  
  });
});
// form Delete Unduhan
$(document).on('click', '.delete-unduh', function() {
$('#footer_action_hapusunudh').text(" Hapus");
$('#footer_action_hapusunduh').removeClass('glyphicon-check');
$('#footer_action_hapusunduh').addClass('glyphicon-trash');
$('.actionBtnw').removeClass('btn-success');
$('.actionBtnw').addClass('btn-danger');
$('.actionBtnw').addClass('hapusunduh');
$('.modal-title').text('Hapus Video');
$('.id_unduh').text($(this).data('id_unduh'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.judulunduh').html($(this).data('name'));
$('#hpsunduh').modal('show');
});

$('.modal-footer').on('click', '.hapusunduh', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delunduh',
    data: {
      'id_unduh': $('.id_unduh').text()
    },
    success: function(data){

    
       var table = $('#tunduh').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsunduh').modal('hide');
                
    }
  });
});

{{-- ajax Form Add Userweb--}}
  $(document).on('click','.create-userweb', function() {
    $('#musr').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah User WEB');
  });
  $("#tambahuser").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
     var type = "POST";
    var url ="adduser";
    var formData = {
      namauser: $('#nmusr').val(),
      email: $('#ema').val(),
      jenisuser: $('#jnsus').val(),
      status: $('#st:checked').val(),
    }
    
    $.ajax({
      type: 'POST',
      url: url,
      data: formData,
       // {
      //   'namauser': $('input[name=namauser]').val(),
      //   'email': $('input[name=email]').val(),
      //   'jnsuser': $('input[name=jenisuser]').val(),
      //   'status': $('input[name=status:checked]').val(),
      // },
      success: function(data){

          $('.errorjdln').addClass('hidden');
          $('.errorlnknd').addClass('hidden');
         
        if ((data.errors)) {
                setTimeout(function () {
                  $('#munduh').modal('show')
                  toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                   }, 500);  
                 if (data.errors.judulunduh) {
                            $('.errorjdln').removeClass('hidden');
                            $('.errorjdln').text(data.errors.judulunduh);
                        }
                         if (data.errors.linkunduh) {
                            $('.errorlnknd').removeClass('hidden');
                            $('.errorlnknd').text(data.errors.linkunduh);
                        }
                       
                       
                        
        } else {
        var table = $('#tuweb').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#usrbaru').trigger('reset');
        $('#musr').modal('hide');
                       
         }
      }
    });
    $('#nmusr').val('');
    $('#ema').val(''); 
    $('#jnsus').val('');
    $('#st').val('');
   
   
  });
// function Edit user web
$(document).on('click', '.edtusr', function() {
$('#footer_action_user').text(" Simpan");
$('#footer_action_user').addClass('glyphicon-check');
$('#footer_action_user').removeClass('glyphicon-trash');
$('.actionBtnus').addClass('btn-success');
$('.actionBtnus').removeClass('btn-danger');
$('.actionBtnus').addClass('ubahuser');
$('.modal-title').text('Ubah User Web');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#id_us').val($(this).data('id_usr'));
$('#nu').val($(this).data('namauser'));
$('#maile').val($(this).data('email'));
$('#jusr').val($(this).data('jnsuser'));
$('#stat').val($(this).data('status'));

$('#edtuser').modal('show');
});

$('.modal-footer').on('click', '.ubahuser', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  var type = "PATCH";
   var id_usr =$('#id_us').val();
    var url ="ubahuser"+"/"+id_usr;
    var Data = {
      id_usr: id_usr,
      namauser: $('#nu').val(),
      email: $('#maile').val(),
      jenisuser: $('#jusr').val(),
      status: $('#stat:checked').val(),
     }
     console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tuweb').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahusr').trigger('reset');
        $('#edtuser').modal('hide');
                       
        

    }
  
  });
});
// form Delete user
$(document).on('click', '.delete-userweb', function() {
$('#footer_action_hapususer').text(" Hapus");
$('#footer_action_hapususer').removeClass('glyphicon-check');
$('#footer_action_hapususer').addClass('glyphicon-trash');
$('.actionBtn_usr').removeClass('btn-success');
$('.actionBtn_usr').addClass('btn-danger');
$('.actionBtn_usr').addClass('hapususer');
$('.modal-title').text('Hapus User WEB');
$('.id_usr').text($(this).data('id_usr'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.judulunduh').html($(this).data('name'));
$('#hpsusr').modal('show');
});

$('.modal-footer').on('click', '.hapususer', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'deluser',
    data: {
      'id_usr': $('.id_usr').text()
    },
    success: function(data){

    
       var table = $('#tuweb').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsusr').modal('hide');
                
    }
  });
});

{{-- ajax Form Add Blog--}}
  $(document).on('click','.create-blog', function() {
    $('#mblog').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Blog');
  });
  $("#tambahblog").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addblog',
      data: {
        'judul': $('input[name=judul]').val(),
        'namauser': $('#nuser').val(),
        
      },
      success: function(data){

        //   $('.errorjdln').addClass('hidden');
        //   $('.errorlnknd').addClass('hidden');
         
        // if ((data.errors)) {
        //         setTimeout(function () {
        //           $('#munduh').modal('show')
        //           toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
        //            }, 500);  
        //          if (data.errors.judulunduh) {
        //                     $('.errorjdln').removeClass('hidden');
        //                     $('.errorjdln').text(data.errors.judulunduh);
        //                 }
        //                  if (data.errors.linkunduh) {
        //                     $('.errorlnknd').removeClass('hidden');
        //                     $('.errorlnknd').text(data.errors.linkunduh);
        //                 }
                       
                       
                        
        // } else {
        var table = $('#tabelblog').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#blogbaru').trigger('reset');
        $('#mblog').modal('hide');
                       
         // }
      }
    });
    $('#judulblog').val('');
    $('#nuser').val(''); 
   
   
   
  });

// function Edit  Blog
$(document).on('click', '.edtblogs', function() {
$('#footer_action_blog').text(" Simpan");
$('#footer_action_blog').addClass('glyphicon-check');
$('#footer_action_blog').removeClass('glyphicon-trash');
$('.actionBtnbg').addClass('btn-success');
$('.actionBtnbg').removeClass('btn-danger');
$('.actionBtnbg').addClass('ubahblog');
$('.modal-title').text('Ubah Blog');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#id_blg').val($(this).data('id_blog'));
$('#jl').val($(this).data('judul'));
$('#use').val($(this).data('rec_usr'));

$('#edtblog').modal('show');
});

$('.modal-footer').on('click', '.ubahblog', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   var type = "PATCH";
   var id_blog =$('#id_blg').val();
    var url ="editblog"+"/"+id_blog;
    var Data = {
      id_blog: id_blog,
      judul: $('#jl').val(),
      rec_usr: $('#use').val(),
      
     }
     console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tabelblog').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahblog').trigger('reset');
        $('#edtblog').modal('hide');
                       
        

    }
  
  });
});
// form Delete blog
$(document).on('click', '.delete-blog', function() {
$('#footer_action_hapusblog').text(" Hapus");
$('#footer_action_hapusblog').removeClass('glyphicon-check');
$('#footer_action_hapusblog').addClass('glyphicon-trash');
$('.actionBtn_blg').removeClass('btn-success');
$('.actionBtn_blg').addClass('btn-danger');
$('.actionBtn_blg').addClass('hapusblog');
$('.modal-title').text('Hapus Blog');
$('.id_blog').text($(this).data('id_blog'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.judul').html($(this).data('name'));
$('#hpsblg').modal('show');
});

$('.modal-footer').on('click', '.hapusblog', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delblog',
    data: {
      'id_blog': $('.id_blog').text()
    },
    success: function(data){

    
       var table = $('#tabelblog').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpsblg').modal('hide');
                
    }
  });
});

{{-- ajax Form Add Klien--}}
  $(document).on('click','.create-klien', function() {
    $('#mklien').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Klien');
  });
  $("#tambahklien").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addklien',
      data: {
       
        'klien': $('input[name=klien]').val(),
        'alamat': $('#alm').val(),
         'telp': $('#tp').val(),
         'email': $('#mail').val(),
      },
      success: function(data){

        //   $('.errorjdln').addClass('hidden');
        //   $('.errorlnknd').addClass('hidden');
         
        // if ((data.errors)) {
        //         setTimeout(function () {
        //           $('#munduh').modal('show')
        //           toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
        //            }, 500);  
        //          if (data.errors.judulunduh) {
        //                     $('.errorjdln').removeClass('hidden');
        //                     $('.errorjdln').text(data.errors.judulunduh);
        //                 }
        //                  if (data.errors.linkunduh) {
        //                     $('.errorlnknd').removeClass('hidden');
        //                     $('.errorlnknd').text(data.errors.linkunduh);
        //                 }
                       
                       
                        
        // } else {
        var table = $('#tabelklien').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#klienbaru').trigger('reset');
        $('#mklien').modal('hide');
                       
         // }
      }
    });
    $('#kl').val('');
    $('#alm').val(''); 
    $('#tp').val('');
    $('#mail').val(''); 
   
   
   
  });

// function Edit  Klien
$(document).on('click', '.edtkliens', function() {
$('#footer_action_klien').text(" Simpan");
$('#footer_action_klien').addClass('glyphicon-check');
$('#footer_action_klien').removeClass('glyphicon-trash');
$('.actionBtnkl').addClass('btn-success');
$('.actionBtnkl').removeClass('btn-danger');
$('.actionBtnkl').addClass('ubahklien');
$('.modal-title').text('Ubah Klien');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#id_kl').val($(this).data('id_klien'));
$('#ki').val($(this).data('klien'));
$('#mt').val($(this).data('alamat'));
$('#lp').val($(this).data('telp'));
$('#imel').val($(this).data('email'))

$('#edtklien').modal('show');
});

$('.modal-footer').on('click', '.ubahklien', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   var type = "PATCH";
   var id_klien =$('#id_kl').val();
    var url ="editklien"+"/"+id_klien;
    var Data = {
      id_klien: id_klien,
      klien: $('#ki').val(),
      alamat: $('#mt').val(),
      telp: $('#lp').val(),
      email: $('#imel').val(),
      
     }
     console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tabelklien').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahklien').trigger('reset');
        $('#edtklien').modal('hide');
                       
        

    }
  
  });
});
// form Delete klien
$(document).on('click', '.delete-kliens', function() {
$('#footer_action_hapusklien').text(" Hapus");
$('#footer_action_hapusklien').removeClass('glyphicon-check');
$('#footer_action_hapusklien').addClass('glyphicon-trash');
$('.actionBtn_kln').removeClass('btn-success');
$('.actionBtn_kln').addClass('btn-danger');
$('.actionBtn_kln').addClass('hapusklien');
$('.modal-title').text('Hapus Blog');
$('.id_klien').text($(this).data('id_klien'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.klien').html($(this).data('name'));
$('#hpskl').modal('show');
});

$('.modal-footer').on('click', '.hapusklien', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  
  $.ajax({
    type: 'POST',
    url: 'delklien',
    data: {
      'id_klien': $('.id_klien').text()
    },
    success: function(data){

    
       var table = $('#tabelklien').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpskl').modal('hide');
                
    }
  });
});

{{-- ajax Form Add Testi--}}
  $(document).on('click','.create-testi', function() {
    $('#mtesti').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Testimoni');
  });
  $("#tambahtesti").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addtesti',
      data: {
        'klien': $('#ket').val(),
        'nama': $('#na').val(),
        'testimoni': $('#ts').val(),
        
      },
      success: function(data){

        //   $('.errorjdln').addClass('hidden');
        //   $('.errorlnknd').addClass('hidden');
         
        // if ((data.errors)) {
        //         setTimeout(function () {
        //           $('#munduh').modal('show')
        //           toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
        //            }, 500);  
        //          if (data.errors.judulunduh) {
        //                     $('.errorjdln').removeClass('hidden');
        //                     $('.errorjdln').text(data.errors.judulunduh);
        //                 }
        //                  if (data.errors.linkunduh) {
        //                     $('.errorlnknd').removeClass('hidden');
        //                     $('.errorlnknd').text(data.errors.linkunduh);
        //                 }
                       
                       
                        
        // } else {
        var table = $('#tabeltesti').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#testibaru').trigger('reset');
        $('#mtesti').modal('hide');
                       
         // }
      }
    });
    $('#ket').val('');
    $('#na').val('');
    $('#ts').val(''); 
   
  });

// function Edit  testi
$(document).on('click', '.edttesti', function() {
$('#footer_action_testi').text(" Simpan");
$('#footer_action_testi').addClass('glyphicon-check');
$('#footer_action_testi').removeClass('glyphicon-trash');
$('.actionBtntm').addClass('btn-success');
$('.actionBtntm').removeClass('btn-danger');
$('.actionBtntm').addClass('ubahtesti');
$('.modal-title').text('Ubah Testimoni');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#id_ts').val($(this).data('id_testi'));
$('#ken').val($(this).data('klien'));
$('#ma').val($(this).data('nama'));
$('#tn').val($(this).data('testimoni'))

$('#edttesti').modal('show');
});

$('.modal-footer').on('click', '.ubahtesti', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   var type = "PATCH";
   var id_testi =$('#id_ts').val();
    var url ="edittesti"+"/"+id_testi;
    var Data = {
      id_testi: id_testi,
      klien: $('#ken').val(),
      nama: $('#ma').val(),
      testimoni: $('#tn').val(),
     
     }
     console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tabeltesti').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahtesti').trigger('reset');
        $('#edttesti').modal('hide');
                       
        

    }
  
  });
});
// form Delete testi
$(document).on('click', '.delete-testi', function() {
$('#footer_action_hapustesti').text(" Hapus");
$('#footer_action_hapustesti').removeClass('glyphicon-check');
$('#footer_action_hapustesti').addClass('glyphicon-trash');
$('.actionBtn_tst').removeClass('btn-success');
$('.actionBtn_tst').addClass('btn-danger');
$('.actionBtn_tst').addClass('hapustesti');
$('.modal-title').text('Hapus Testimoni');
$('.id_testi').text($(this).data('id_testi'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.klien').html($(this).data('name'));
$('#hpstes').modal('show');
});

$('.modal-footer').on('click', '.hapustesti', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'deltesti',
    data: {
      'id_testi': $('.id_testi').text()
    },
    success: function(data){

    
       var table = $('#tabeltesti').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpstes').modal('hide');
                
    }
  });
});

{{-- ajax Form Add Partner--}}
  $(document).on('click','.create-partner', function() {
    $('#mpart').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Tambah Partner');
  });
  $("#tambahpartner").click(function() {
    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    $.ajax({
      type: 'POST',
      url: 'addpartner',
      data: {
        'partner': $('#pp').val(),
        'alamat': $('#at').val(),
        'telp': $('#tep').val(),
        'email': $('#ems').val(),
        
      },
      success: function(data){

        //   $('.errorjdln').addClass('hidden');
        //   $('.errorlnknd').addClass('hidden');
         
        // if ((data.errors)) {
        //         setTimeout(function () {
        //           $('#munduh').modal('show')
        //           toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
        //            }, 500);  
        //          if (data.errors.judulunduh) {
        //                     $('.errorjdln').removeClass('hidden');
        //                     $('.errorjdln').text(data.errors.judulunduh);
        //                 }
        //                  if (data.errors.linkunduh) {
        //                     $('.errorlnknd').removeClass('hidden');
        //                     $('.errorlnknd').text(data.errors.linkunduh);
        //                 }
                       
                       
                        
        // } else {
        var table = $('#tabelpartner').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully added Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#partnerbaru').trigger('reset');
        $('#mpart').modal('hide');
                       
         // }
      }
    });
    $('#pp').val('');
    $('#at').val('');
    $('#tep').val('');
    $('#ems').val(''); 
   
  });

// function Edit  partner
$(document).on('click', '.edtpartner', function() {
$('#footer_action_partner').text(" Simpan");
$('#footer_action_partner').addClass('glyphicon-check');
$('#footer_action_partner').removeClass('glyphicon-trash');
$('.actionBtngt').addClass('btn-success');
$('.actionBtngt').removeClass('btn-danger');
$('.actionBtngt').addClass('ubahpartner');
$('.modal-title').text('Ubah Partner');
$('.deleteContent').hide();
$('.form-horizontal').show();
$('#ppt').val($(this).data('id_partner'));
$('#pt').val($(this).data('partner'));
$('#alt').val($(this).data('alamat'));
$('#tlp').val($(this).data('telp'));
$('#emaile').val($(this).data('email'));

$('#edtpartner').modal('show');
});

$('.modal-footer').on('click', '.ubahpartner', function() {

  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
   var type = "PATCH";
   var id_partner =$('#ppt').val();
    var url ="editpartner"+"/"+id_partner;
    var Data = {
      id_partner: id_partner,
      partner: $('#pt').val(),
      alamat: $('#alt').val(),
      telp: $('#tlp').val(),
      email: $('#emaile').val(),
     
     }
     console.log(Data);
  $.ajax({
     type: type,
    url: url,

    data:Data,
    success: function(data) {
         // $('.errorjns').addClass('hidden');
         //  $('.errorkon').addClass('hidden');
         //  $('.errorIkon').addClass('hidden');
         //  $('.errorurut').addClass('hidden');
   //   console.log(data);
        
        var table = $('#tabelpartner').DataTable();
        table.ajax.reload();  

        console.log(data);
        toastr.success("Successfully updated Post! ", 'Success Alert!' ,{timeOut:2000});
        $('#ubahpartner').trigger('reset');
        $('#edtpartner').modal('hide');
                       
        

    }
  
  });
});
// form Delete testi
$(document).on('click', '.delete-partner', function() {
$('#footer_action_hapuspartner').text(" Hapus");
$('#footer_action_hapuspartner').removeClass('glyphicon-check');
$('#footer_action_hapuspartner').addClass('glyphicon-trash');
$('.actionBtn_prt').removeClass('btn-success');
$('.actionBtn_prt').addClass('btn-danger');
$('.actionBtn_prt').addClass('hapuspartner');
$('.modal-title').text('Hapus Partner');
$('.id_partner').text($(this).data('id_partner'));
$('.deleteContent').show();
$('.form-horizontal').hide();
$('.partner').html($(this).data('name'));
$('#hpspart').modal('show');
});

$('.modal-footer').on('click', '.hapuspartner', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delpartner',
    data: {
      'id_partner': $('.id_partner').text()
    },
    success: function(data){

    
       var table = $('#tabelpartner').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#hpspart').modal('hide');
                
    }
  });
});






// form Delete gambar produk
$(document).on('click', '.delete-gambar', function() {
$('#footer_action_hpsgbr').text(" Hapus");
$('#footer_action_hpsgbr').removeClass('glyphicon-check');
$('#footer_action_hpsgbr').addClass('glyphicon-trash');
$('.actionBtn_hapusgambar').removeClass('btn-success');
$('.actionBtn_hapusgambar').addClass('btn-danger');
$('.actionBtn_hapusgambar').addClass('hapusgambar');
$('.modal-title').text('Hapus Gambar');
$('.id_gbr').text($(this).data('id_gbr'));
$('.deleteContent').show();
$('.form-horizontal').hide();
// $('.lokasi').html($(this).data('name'));
$('#gbrhapus').modal('show');
});

$('.modal-footer').on('click', '.hapusgambar', function(){
  $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  $.ajax({
    type: 'POST',
    url: 'delgbr',
    data: {
      'id_gbr': $('.id_gbr').text()
    },
    success: function(data){

    
       var table = $('#tproduk').DataTable();
        table.ajax.reload();  

        console.log(data);
          toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
        $('#gbrhapus').modal('hide');
                
    }
  });
});
// form Delete gambar user
// $(document).on('click', '.delete-gambarusr', function() {
// $('#footer_action_hpsgbrusr').text(" Hapus");
// $('#footer_action_hpsgbrusr').removeClass('glyphicon-check');
// $('#footer_action_hpsgbrusr').addClass('glyphicon-trash');
// $('.actionBtn_hapususrgbr').removeClass('btn-success');
// $('.actionBtn_hapususrgbr').addClass('btn-danger');
// $('.actionBtn_hapususrgbr').addClass('hapusgambaruser');
// $('.modal-title').text('Hapus Gambar Userweb');
// $('.id_gbr').text($(this).data('id_gbr'));
// $('.deleteContent').show();
// $('.form-horizontal').hide();
// // $('.lokasi').html($(this).data('name'));
// $('#userpichapus').modal('show');
// });

// $('.modal-footer').on('click', '.hapusgambaruser', function(){
//   $.ajaxSetup({
//       headers:{
//         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//       }
//     })
//    var id_gbr =$('#id_gu').val();
//     var url ="delgbruser"+"/"+id_gbr;
//   $.ajax({
//     type: 'DELETE',

//     url: url,
//     data: {
//       'id_gbr': $('.id_gbr').text()
//     },
//     success: function(data){

    
//        var table = $('#tuweb').DataTable();
//         table.ajax.reload();  

//         console.log(data);
//           toastr.success('Successfully deleted Post! ', 'Success Alert', {timeOut: 2000});
      
//         $('#userpichapus').modal('hide');
                
//     }
//   });
// });
 </script>
   </body>
</html>
