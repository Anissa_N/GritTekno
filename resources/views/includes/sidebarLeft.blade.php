 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
      <p align="center">
      @if(Auth::guest())
      <p align="center"><a href="{{route('login')}}">Login</a> 
      <a href="{{route('register')}}">Register</a> 
      </p>
      @else
       <a> {{ Auth::user()->name }} <i class="fa fa-circle text-success"></i> Online</a>
      @endif
      </p>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
         <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
           <!--  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
 -->          </a>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-reorder"></i> <span>Referensi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="inisial"><i class="fa fa-circle-o"></i>Inisialisasi</a></li>
            <li><a href="admin"><i class="fa fa-circle-o"></i>Profil Perusahaan</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-reorder"></i> <span>Konten Web</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="produk"><i class="fa fa-circle-o"></i> Produk</a></li>
            <li ><a href="projek"><i class="fa fa-circle-o"></i> Projek </a></li>
            <li ><a href="servis"><i class="fa fa-circle-o"></i> Servis</a></li>
            <li ><a href="blogadmin"><i class="fa fa-circle-o"></i> Blog </a></li>
            <li ><a href="klientesti"><i class="fa fa-circle-o"></i> Klien & Testi</a></li>
            <li ><a href="partner"><i class="fa fa-circle-o"></i> Partner </a></li>
            <li ><a href="unduhan"><i class="fa fa-circle-o"></i> Unduhan</a></li>
          </ul>
        </li>
        <li class="active treeview">
        <li>
          <a href="userweb">
            <i class="fa fa-dashboard"></i> <span>User Web</span>
           <!--  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
 -->          </a>
        </li>
        </li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
