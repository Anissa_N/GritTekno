<!DOCTYPE html>
<html>
<head>
@extends('layouts.adm')
 @include('includes.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
@include('includes.header')
@include('includes.sidebarleft')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content ">
     

        <div class="col-md-12 ">
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Produk</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div class="row ">
              <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover  text-center" width="100%"  id="tproduk">
                  <thead>
                  <tr >
                    <th  class="text-center"  width="5%" >
                     <a href="#" class="create-produk btn btn-success btn-sm">
                     <i class="glyphicon glyphicon-plus"></i> </a>
                    
                    </th>
                    <th>No</th>
                    <th>Jenis</th>
                    <th>Produk</th>
                    <th>Gambar</th>
                 
                  </tr>
                  </thead>
                  <tbody>
              
                  </tbody>
                </table>
 
                

                </div>
                </div>
            </div>
            <!-- ./box-body -->
             
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      
 @include('layouts.modal')
       
       
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('includes.sidebarRight')
  @include('includes.footer')
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->
@include('includes.script')
</body>
</html>
