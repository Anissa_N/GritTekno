<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Laravel/Flexor Bootstrap Theme</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <!-- Fav and touch icons -->
  <link rel="shortcut icon" href="img/icons/favicon.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/72x72.png">
  <link rel="apple-touch-icon-precomposed" href="img/icons/default.png">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/owl.theme.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/owl.transitions.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!--Your custom colour override - predefined colours are: colour-blue.css, colour-green.css, colour-lavander.css, orange is default-->
  <link href="#" id="colour-scheme" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Flexor
    Theme URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body class="page-index has-hero">
  <!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->
  <div id="background-wrapper" class="buildings" data-stellar-background-ratio="0.1">

    <!-- ======== @Region: #navigation ======== -->
    <div id="navigation" class="wrapper">
     <!--Hidden Header Region-->
      <div class="header-hidden collapse">
        <div class="header-hidden-inner container">
          <div class="row">
            <div class="col-md-3">
              <h3>
                  About Us
                </h3>
              <p>Flexor is a super flexible responsive theme with a modest design touch.</p>
              <a href="about.html" class="btn btn-more"><i class="fa fa-plus"></i> Learn more</a>
            </div>
            <div class="col-md-3">
              <!--@todo: replace with company contact details-->
              <h3>
                  Contact Us
                </h3>
              <address>
                  <strong>Flexor Bootstrap Theme Inc</strong>
                  <abbr title="Address"><i class="fa fa-pushpin"></i></abbr>
                  Sunshine House, Sunville. SUN12 8LU.
                  <br>
                  <abbr title="Phone"><i class="fa fa-phone"></i></abbr>
                  019223 8092344
                  <br>
                  <abbr title="Email"><i class="fa fa-envelope-alt"></i></abbr>
                  info@flexorinc.com
                </address>
            </div>
            <div class="col-md-6">
              <!--Colour & Background Switch for demo - @todo: remove in production-->
              <h3>
                  Theme Variations
                </h3>
              <div class="switcher">
                <div class="cols">
                  Backgrounds:
                  <br>
                  <a href="#benches" class="background benches active" title="Benches">Benches</a> <a href="#boots" class="background boots " title="Boots">Boots</a> <a href="#buildings" class="background buildings " title="Buildings">Buildings</a>
                  <a
                    href="#city" class="background city " title="City">City</a> <a href="#metro" class="background metro " title="Metro">Metro</a>
                </div>
                <div class="cols">
                  Colours:
                  <br>
                  <a href="#orange" class="colour orange active" title="Orange">Orange</a> <a href="#green" class="colour green " title="Green">Green</a> <a href="#blue" class="colour blue " title="Blue">Blue</a> <a href="#lavender" class="colour lavender "
                    title="Lavender">Lavender</a>
                </div>
              </div>
              <p>
                <small>Selection is not persistent.</small>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!--Header & navbar-branding region-->
      <div class="header">
        <div class="header-inner container">
          <div class="row">
            <div class="col-md-8">
              <!--navbar-branding/logo - hidden image tag & site name so things like Facebook to pick up, actual logo set via CSS for flexibility -->
              <a class="navbar-brand" href="index.html" title="Home">
                <h1 class="hidden">
                    <img src="flexor/img/logo.png" alt="Flexor Logo">
                    Flexor
                  </h1>
              </a>
              <div class="navbar-slogan">
                Responsive HTML Theme
                <br> By BootstrapMade.com
              </div>
            </div>
            <!--header rightside-->
            <div class="col-md-4">
              <!--user menu-->
              <!-- <ul class="list-inline user-menu pull-right">
                <li class="user-button"><a class="btn btn-primary btn-hh-trigger" role="button" data-toggle="collapse" data-target=".header-hidden">Open</a></li>
              </ul>
              <ul class="list-inline user-menu pull-right">
              @if (Auth::guest()) -->
                           <!--  <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form> -->
                                       <!--  <a href="{{url('/changePassword')}}">
                                        Ubah Password
                                       </a> -->
                                   <!--  </li>
                                </ul>
                            </li>
                        @endif -->
                <!-- <li class="user-register"><i class="fa fa-edit text-primary "></i> <a href="" class="text-uppercase">Register</a></li>
                <li class="user-login"><i class="fa fa-sign-in text-primary"></i> <a href="" class="text-uppercase">Login</a></li> -->
              <!-- </ul> -->
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="navbar navbar-default">
          <!--mobile collapse menu button-->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <!--social media icons-->
          <div class="navbar-text social-media social-media-inline pull-right">
        @if(count($sosmed)>0)
        @foreach($sosmed->all() as $sosmed)
        <a href="{{$sosmed->dtlsosmed}}"><i class="fa {{$sosmed->ikon}}"></i></a>
        
          @endforeach
          @endif
          </div>
          <!--everything within this div is collapsed on mobile-->
          <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav" id="main-menu">
              <li class="icon-link">
                <a href="pageheader"><i class="fa fa-home"></i></a>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages<b class="caret"></b></a>
                <!-- Dropdown Menu -->
                <ul class="dropdown-menu">
                  <li class="dropdown-header">Flexor Version Pages</li>
                  <li><a href="elementpage" tabindex="-1" class="menu-item">Elements</a></li>
                  <li><a href="aboutpage" tabindex="-1" class="menu-item">About / Inner Page</a></li>
                  <li><a href="login" tabindex="-1" class="menu-item">Login</a></li>
                  <li><a href="regist" tabindex="-1" class="menu-item">Sign-Up</a></li>
                  <li class="dropdown-footer">Dropdown footer</li>
                </ul>
              </li>
               <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Referensi<b class="caret"></b></a>
                <!-- Dropdown Menu -->
                <ul class="dropdown-menu">
                 
                  <li><a href="#" tabindex="-1" class="menu-item">#</a></li>
                  <li><a href="#" tabindex="-1" class="menu-item">#</a></li>
                </ul>
              </li>
               
               <li class="dropdown dropdown-mm">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mega Menu<b class="caret"></b></a>
                <!-- Dropdown Menu -->
                <ul class="dropdown-menu dropdown-menu-mm dropdown-menu-persist">
                  <li class="row">
                    <ul class="col-md-6">
                      <li class="dropdown-header">Websites and Apps</li>
                      <li><a href="#">Analysis and Planning</a></li>
                      <li><a href="#">User Experience / Information Architecture</a></li>
                      <li><a href="#">User Interface Design / UI Design</a></li>
                      <li><a href="#">Code &amp; Development / Implementation &amp; Support</a></li>
                    </ul>
                    <ul class="col-md-6">
                      <li class="dropdown-header">Enterprise solutions</li>
                      <li><a href="#">Business Analysis</a></li>
                      <li><a href="#">Custom UX Consulting</a></li>
                      <li><a href="#">Quality Assurance</a></li>
                    </ul>
                  </li>
                  
                </ul>
              </li>
             
              
            </ul>
          </div>
          <!--/.navbar-collapse -->
        </div>
      </div>
    </div>
    <div class="hero" id="highlighted">
      <div class="inner">
        <!--Slideshow-->
        <div id="highlighted-slider" class="container">
          <div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp"}'>
            <!--Slideshow content-->
            <!--Slide 1-->
        @if(count($slider)>0)
        @foreach($slider->all() as $slid)
          @if($slid->aktif == 1)
            <div class="item">
              <div class="row">
                <div class="col-md-6 {{$slid->posisi}} item-caption">
                  <h2 class="h1 text-weight-light">
                     {{$slid->judulsld}}
                    </h2>
                  <h4>
                      {{$slid->subjudul}}
                    </h4>
                  <p>{{$slid->prakata}}</p>
                  <a href="https://bootstrapmade.com" class="btn btn-more btn-lg i-right">Buy Now <i class="fa fa-plus"></i></a>

                </div>
                <div class="{{$slid->posisigambar}}">
                  <img src="{{$slid->lokasi}}" class="center-block img-responsive">
                </div>
              </div>
            </div>
           @else
           @endif
        @endforeach
        @endif
         
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- ======== @Region: #content ======== -->
    <!-- ======== @Region: #content ======== -->
  <div id="content">
    <!-- Mission Statement -->
    <div class="mission text-center block block-pd-sm block-bg-noise">
     @if(count($profile)>0)
        @foreach($profile->all() as $prof)
      <div class="container">

        <h2 class="text-shadow-white">
            {{$prof->slogan}}
            <a href="aboutpage" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
          </h2>
      </div>
      @endforeach
      @endif
    </div>
    <!--Showcase-->
    <div class="showcase block block-border-bottom-grey">
      <div class="container">
        <h2 class="block-title">
            Showcase
          </h2>
        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a
          sit amet mauris.</p>
        <div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "itemsScaleUp":true}'>
          @if(count($produk)>0)
        @foreach($produk->take(4) as $prd)
          <div class="item">
          @foreach($prd->gambar->shuffle()->take(1) as $gbr)
            <a href="{{$gbr->lokasi}}"  value="{{$prd->id_prod}}" class="overlay-wrapper">
                <img src="{{$gbr->lokasi}}" alt="{{$prd->produk}}" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">{{$prd->produk}}</span> </span>
                </span>
              </a>
            @endforeach
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">{{$prd->deskripsi}}</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
            </div>
          </div>
          
        @endforeach
        @endif
        </div>
      </div>
    </div>
    <!-- Services -->
    <div class="services block block-bg-gradient block-border-bottom">
      <div class="container">
        <h2 class="block-title">
            Our Services
          </h2>
        <div class="row">
        @if(count($servis)>0)
        @foreach($servis->take(3) as $srv)

          <div class="col-md-4 text-center">
            <span class="fa-stack fa-5x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="{{$srv->ikon}} fa-stack-1x fa-inverse"></i> </span>
            <h4 class="text-weight-strong">
               {{$srv->servis}}
              </h4>
            <p>{{$srv->deskripsi}}</p>
            <p>
              <a href="#" class="btn btn-more i-right">Learn More <i class="fa fa-angle-right"></i></a>
            </p>
          </div>
         @endforeach
         @endif
         
        </div>
      </div>
    </div>
    <!-- Pricing -->
    <div class="block-contained">
      <h2 class="block-title">
          Our Plans
        </h2>
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default panel-pricing text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Flex<em>Starter</em>
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">19.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>3 User Accounts</li>
                <li>3 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>5GB of space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default panel-pricing panel-pricing-highlighted text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Team<em>Starter</em>
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">49.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>50 User Accounts</li>
                <li>50 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>Unlimited space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
        <div class="col-md-3 text-center">
          <div class="panel panel-default panel-pricing panel-pricing-highlighted text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Enterprise
                  <span class="panel-pricing-popular"><i class="fa fa-thumbs-up"></i> Popular</span>
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">199.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>100 User Accounts</li>
                <li>100 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>Unlimited space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
        <div class="col-md-3 text-center">
          <div class="panel panel-default panel-pricing text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Corporate
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">299.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>1000 User Accounts</li>
                <li>1000 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>Unlimited space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!--
Background image callout with CSS overlay
Usage: data-block-bg-img="IMAGE-URL" to apply a background image clearly via jQuery .block-bg-overlay = overlays the background image, colour is inherited from block-bg-* classes .block-bg-overlay-NUMBER = determines opcacity value of overlay from 1-9 (default is 5) ie. .block-bg-overlay-2 or .block-bg-overlay-6
-->
    <div class="block block-pd-sm block-bg-grey-dark block-bg-overlay block-bg-overlay-6 text-center" data-block-bg-img="https://picjumbo.imgix.net/HNCK1088.jpg?q=40&amp;w=1650&amp;sharp=30" data-stellar-background-ratio="0.3">
      <h2 class="h-xlg h1 m-a-0">
          <span data-counter-up>100,000,0</span>s
        </h2>
      <h3 class="h-lg m-t-0 m-b-lg">
          Of Happy Customers!
        </h3>
      <p>
        <a href="#" class="btn btn-more btn-lg i-right">Join them today! <i class="fa fa-angle-right"></i></a>
      </p>
    </div>
    <!--Customer testimonial & Latest Blog posts-->
    <div class="testimonials block-contained">
      <div class="row">
      @if(count($testi)>0)
        @foreach($testi->take(1) as $tes)
        <!--Customer testimonial-->
        <div class="col-md-6 m-b-lg">
          <h3 class="block-title">
              Testimonials
            </h3>
          <blockquote>
            <p>
              {{$tes->testimoni}}
            </p>
            <img src="{{$tes->lokasi}}" alt="{{$tes->klien}}">
            <small>
                <strong>{{$tes->nama}}</strong>
                <br>
               {{$tes->klien}}
              </small>
          </blockquote>
        </div>
        @endforeach
        @endif
        <!--Latest Blog posts-->
        <div class="col-md-6 blog-roll">
          <h3 class="block-title">
              Latest From Our Blog
            </h3>
          <!-- Blog post 1-->
            @if(count($blog)>0)
        @foreach($blog->take(2) as $bl)
       
          <div class="media">
            <div class="media-left hidden-xs">
              <!-- Date desktop -->
              <div class="date-wrapper"> <span class="date-m">Feb</span> <span class="date-d">01</span> </div>
            </div>
            <div class="media-body">
              <h4 class="media-heading">
                  <a href="#" class="text-weight-strong">{{$bl->judul}}</a>
                </h4>
              <!-- Meta details mobile -->
              <ul class="list-inline meta text-muted visible-xs">
                <li><i class="fa fa-calendar"></i> <span class="visible-md">Created:</span> Fri 1st Feb 2013</li>
                <li><i class="fa fa-user"></i> <a href="#">Joe</a></li>
              </ul>
              <p>
                {{$bl->isi_blog}}
                <a href="blogpage/{{$bl->id_blog}}">Read more <i class="fa fa-angle-right"></i></a>
              </p>
            </div>
          </div>
          @endforeach
          @endif
         
        </div>
      </div>
    </div>
  </div>
 <div class="block block-pd-sm block-bg-primary">
    <div class="container">
      <div class="row">
        <h3 class="col-md-4">
            Some of our Clients
          </h3>
        <div class="col-md-8">
       
          <div class="row">
            <!--Client logos should be within a 120px wide by 60px height image canvas-->
              @if(count($klien)>0)
        @foreach($klien->take(6) as $kli)
       
        
            <div class="col-xs-6 col-md-2">
              <a href="https://bootstrapmade.com" title="{{$kli->klien}}">
                  <img src="{{$kli->lokasi}}" alt="{{$kli->lokasi}}" class="img-responsive">
                </a>
            </div>
           @endforeach
           @endif
            
         
          </div>
        </div>
      </div>
    </div>
  </div>

 
  <!-- ======== @Region: #footer ======== -->
  <footer id="footer" class="block block-bg-grey-dark" data-block-bg-img="img/bg_footer-map.png" data-stellar-background-ratio="0.4">
    <div class="container">

      <div class="row" id="contact">

        <div class="col-md-3">
          <address>
              <strong>Flexor Bootstrap Theme Inc</strong>
              @if(count($kontak)>0)
              @foreach($kontak->all() as $kont)
              <br>
              <i class="fa {{$kont->ikon}}"></i> {{$kont->dtlkontak}}
                  
              @endforeach
              @endif
          </address>
        </div>

        <div class="col-md-6">
          <h4 class="text-uppercase">
              Contact Us
            </h4>
          <div class="form">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <form action="" method="post" role="form" class="contactForm">
              <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>
        </div>

        <div class="col-md-3">
          <h4 class="text-uppercase">
              Follow Us On:
            </h4>
          <!--social media icons-->
          <div class="social-media social-media-stacked">
            <!--@todo: replace with company social media details-->
            <a href="#"><i class="fa fa-twitter fa-fw"></i> Twitter</a>
            <a href="#"><i class="fa fa-facebook fa-fw"></i> Facebook</a>
            <a href="#"><i class="fa fa-linkedin fa-fw"></i> LinkedIn</a>
            <a href="#"><i class="fa fa-google-plus fa-fw"></i> Google+</a>
          </div>
        </div>

      </div>

      <div class="row subfooter">
        <!--@todo: replace with company copyright details-->
        <div class="col-md-7">
          <p>Copyright © Flexor Theme</p>
          <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Flexor
            -->
            <a href="https://bootstrapmade.com/">Free Bootstrap Templates</a> by BootstrapMade.com
          </div>
        </div>
        <div class="col-md-5">
          <ul class="list-inline pull-right">
            <li><a href="#">Terms</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">Contact Us</a></li>
          </ul>
        </div>
      </div>

      <a href="#top" class="scrolltop">Top</a>

    </div>
  </footer>

  <!-- Required JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/stellar/stellar.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="contactform/contactform.js"></script>

  <!-- Template Specisifc Custom Javascript File -->
  <script src="js/custom.js"></script>

  <!--Custom scripts demo background & colour switcher - OPTIONAL -->
  <script src="js/color-switcher.js"></script>

  <!--Contactform script -->
  <script src="contactform/contactform.js"></script>

</body>

</html>
