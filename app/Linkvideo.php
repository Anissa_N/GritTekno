<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linkvideo extends Model
{
     protected $table = 'link_video';
     protected $primaryKey = 'id_vid';
     public $timestamps = false;

}
