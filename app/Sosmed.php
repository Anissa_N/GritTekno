<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sosmed extends Model
{
     protected $table = 'sosmed';

     protected $primaryKey = 'id_sosmed';
     public $timestamps = false;
     
}
