<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
     protected $table = 'office';

     protected $primaryKey = 'id_off';
     public $timestamps = false;
}
