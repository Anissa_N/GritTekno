<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Klien extends Model
{
     protected $table = 'klien';
      protected $primaryKey = 'id_klien';
     public $timestamps = false;
       function gambar(){
     	return $this->hasOne('App\Gambar', 'id_klien');
     }
}
