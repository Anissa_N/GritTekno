<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userweb extends Model
{
     protected $table = 'usr_web';

     protected $primaryKey = 'id_usr';
     public $timestamps = false;
       function gambar(){
     	return $this->hasOne('App\Gambar', 'id_usr');
     }
}


