<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unduhan extends Model
{
    protected $table = 'unduh';

     protected $primaryKey = 'id_unduh';
     public $timestamps = false;
}
