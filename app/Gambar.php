<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
     protected $table = 'gbr';
     public $timestamps = false;
      protected $primaryKey = 'id_gbr';
     protected $fillable = ['lokasi', 'id_prod', 'id_sld', 'id_prjt'];

     public function Produk(){
     	return $this->belongsTo('App\Produk', 'id_prod', 'id_prod');
     }
}
