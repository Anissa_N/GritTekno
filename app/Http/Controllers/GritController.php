<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Hash;
use Auth;
use Response;
use Validator;
use App\Kontak;
use App\Slider;
use App\Sosmed;
use App\Gambar;
use App\Produk;
use App\Projek;
use App\Klien;
use App\Blog;
use App\Service;
use App\Profile;
use App\Testi;
use App\Office;
use App\Jenis;
use App\Linkvideo;
use App\Partner;
use App\Unduhan;
use App\Userweb;
use App\JenisUser;

class GritController extends Controller
{
     public function about(){
    $klien = Klien::join ('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
    $sosmed = Sosmed::all();
    $kontak = Kontak::all();
    $gambar = Gambar::all();
    $profile = Profile::join('gbr', 'gbr.id_prof', '=', 'profile.id_prof')->select('gbr.*', 'profile.*')->get();
      return view('template.about', ['sosmed'=>$sosmed,'kontak'=>$kontak,'klien'=>$klien,'profile'=>$profile]);
   

    }
      public function baru($id){
     $klien = Klien::join ('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
     $sosmed = Sosmed::all();
      $kontak = Kontak::all();
      $gambar = Gambar::all();
      $blog = Blog::find($id);
      $bloge = Blog::join('usr_web', 'usr_web.id_usr', '=', 'blog.rec_usr')->select('usr_web.*', 'blog.*')->get();
      return view('template.blog', ['sosmed'=>$sosmed,'kontak'=>$kontak,'klien'=>$klien,'blog'=>$blog,'bloge'=>$bloge]);
    }

     public function element(){
     
      return view('template.element');
    }
     public function login(){
     
      return view('template.login');
    }
     public function regis(){
     
      return view('template.regis');
    }

public function admn()
    { $profile = Profile::all();
      $gambar = Gambar::all();
      $jenis = Jenis::all();
      $kontak = Kontak::all();
      $sosmed = Sosmed::all();
      $office = Office::all();
      $produk = Produk::all();
      $user = Userweb::all();
      $userweb = Userweb::all();
      $klien = Klien::all();
      $jenisuser = JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
    	return view('template.admin', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'klien'=>$klien,'user'=>$user,'userweb'=>$userweb,'produk'=>$produk,'gambar'=>$gambar,'jenis'=>$jenis,'sosmed'=>$sosmed,'kontak'=>$kontak,'profile'=>$profile,'office'=>$office]);
}
public function inisialisasi()

    { $profile = Profile::all();
      $userweb = Userweb::all();
      $klien = Klien::all();
      $jenis = Jenis::all();
      $user = Userweb::all();
      $kontak = Kontak::all();
      $sosmed = Sosmed::all();
      $office = Office::all();
      $gambar = Gambar::all();
      $produk = Produk::all();
      $jenisuser = JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
      $slider = Slider::join ('gbr','gbr.id_sld', '=', 'slider.id_sld')->select('gbr.*', 'slider.*')->orderby('urut')->get();

      return view('template.inisialisasi', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'gambar'=>$gambar,'jenis'=>$jenis,'sosmed'=>$sosmed,'kontak'=>$kontak,'profile'=>$profile,'office'=>$office,'slider'=>$slider]);
}
public function pageproduk()
    { 
      $jenis = Jenis::all();
      $gambar = Gambar::all();
      $produk = Produk::all();
      $user = Userweb::all();
      $userweb = Userweb::all();
      $klien= Klien::all();
      $jenisuser = JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
      // $produk = Produk::join ('gbr','gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get();

      return view('template.PageProduct', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'klien'=>$klien,'userweb'=>$userweb,'jenis'=>$jenis,'produk'=>$produk,'gambar'=>$gambar]);
}
public function pageprojek()
    { 
      $jenis = Jenis::all();
      $projek = Projek::all();
       $gambar = Gambar::all();
       $produk = Produk::all();
       $userweb = Userweb::all();
       $klien = Klien::all();
      $user = Userweb::all();
      $jenisuser = JenisUser::all();
      $blog= Blog::all();

      return view('template.PageProject', ['blog'=>$blog,'jenisuser'=>$jenisuser,'klien'=>$klien,'userweb'=>$userweb,'user'=>$user,'produk'=>$produk,'gambar'=>$gambar,'jenis'=>$jenis,'projek'=>$projek]);
}
public function pageservis()
    { 
      $jenis = Jenis::all();
      $gambar = Gambar::all();
      $servis = Service::all();
      $produk = Produk::all();
      $user = Userweb::all();
      $klien = Klien::all();
      $user = Userweb::all();
      $userweb = Userweb::all();
      $jenisuser = JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
      return view('template.PageServis', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'userweb'=>$userweb,'klien'=>$klien,'produk'=>$produk,'gambar'=>$gambar,'jenis'=>$jenis,'servis'=>$servis]);
}
public function pageunduh()
    { 
      $jenis = Jenis::all();
      $userweb = Userweb::all();
      $produk = Produk::all();
      $unduh = Unduhan::all();
      $gambar = Gambar::all();
      $klien = Klien::all();
      $user = Userweb::all();
      $jenisuser =JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
      return view('template.PageUnduh', ['projek'=>$projek,'blog'=>$blog,'user'=>$user,'jenisuser'=>$jenisuser,'userweb'=>$userweb,'klien'=>$klien,'gambar'=>$gambar,'produk'=>$produk,'jenis'=>$jenis,'unduh'=>$unduh]);
}
public function pagepartner()
    { 
      $jenis = Jenis::all();
      $produk = Produk::all();
      $partner = Partner::all();
      $userweb = Userweb::all();
      $gambar = Gambar::all();
      $user = Userweb::all();
      $klien = Klien::all();
      $jenisuser = JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
      return view('template.PagePartner', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'gambar'=>$gambar,'user'=>$user,'produk'=>$produk,'userweb'=>$userweb,'klien'=>$klien,'jenis'=>$jenis,'partner'=>$partner]);
}
public function pageblog()
    { 
      $jenis = Jenis::all();
      $gambar = Gambar::all();
      $blog = Blog::all();
      $produk = Produk::all();
      $userweb = Userweb::all();
      $klien = Klien::all();
      $user = Userweb::all();
      $jenisuser = JenisUser::all();
      $projek = Projek::all();
      return view('template.PageBlog', ['projek'=>$projek,'jenisuser'=>$jenisuser,'user'=>$user,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'jenis'=>$jenis,'blog'=>$blog,'gambar'=>$gambar]);
}
public function pageklientesti()
    { 
      $jenis = Jenis::all();
      $gambar = Gambar::all();
      $produk = Produk::all();
      $userweb = Userweb::all();
      $klien = Klien::all();
      $testi = Testi::all();
      $user = Userweb::all();
      $jenisuser = JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
      return view('template.PageKlienTesti', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'testi'=>$testi,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'jenis'=>$jenis,'gambar'=>$gambar]);
}
public function pageuserweb()
    { 
      $userweb = Userweb::all();
      $userweb = Userweb::all();
      $jenis = Jenis::all();
      $gambar = Gambar::all();
      $produk = Produk::all();
      $klien =Klien::all();
      $user = Userweb::all();
      $jenisuser = JenisUser::all();
      $projek = Projek::all();
      $blog = Blog::all();
      return view('template.PageUserweb', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'jenis'=>$jenis,'gambar'=>$gambar,'userweb'=>$userweb]);
}

  public function sosmed()
    {
    $sosmed = Sosmed::all();
    $kontak = Kontak::all();
    $servis = Service::all()->shuffle();
    $gambar = Gambar::all(); 
    $testi = Testi::join ('gbr', 'gbr.id_testi', '=', 'testi.id_testi')->join('klien', 'klien.id_klien', '=', 'testi.id_klien')->get()->shuffle();
    $slider = Slider::join ('gbr','gbr.id_sld', '=', 'slider.id_sld')->select('gbr.*', 'slider.*')->orderby('urut')->get();
    $produk = Produk::all()->shuffle();
    // $produk = Produk::join ('gbr', 'gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get()->shuffle();
    $klien = Klien::join ('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
    $blog = Blog::orderby('rec_wkt', 'desc')->get();
    $profile = Profile::all();
   return view('template.pageheader', ['sosmed'=>$sosmed,'kontak'=>$kontak,'slider'=>$slider,'produk'=>$produk,'klien'=>$klien,'blog'=>$blog,'servis'=>$servis,'profile'=>$profile,'testi'=>$testi]);
    }

    //array datatables
    function bebas(){
      $datan= Kontak::all();
      $baris = array();
      $a = 0 ;
      foreach ($datan as $datas) {
         $baris[$a]['id_ktk'] = $datas->id_ktk;
         $baris[$a]['jnskontak'] = $datas->jnskontak;
         $baris[$a]['dtlkontak'] = $datas->dtlkontak;  
         $baris[$a]['ikon'] = $datas->ikon;
         $baris[$a]['urut'] = $datas->urut;
      $a++;
       } 
       $data = array ('data'=>$baris);
       return response()->json($data);
    }
    function media(){
      $datan= Sosmed::all();
      $baris = array();
      $a = 0 ;
      foreach ($datan as $datas) {
         $baris[$a]['id_sosmed'] = $datas->id_sosmed;
         $baris[$a]['jnssosmed'] = $datas->jnssosmed;
         $baris[$a]['dtlsosmed'] = $datas->dtlsosmed;  
         $baris[$a]['ikon'] = $datas->ikon;
         $baris[$a]['urut'] = $datas->urut;
      $a++;
       } 
       $data = array ('data'=>$baris);
       return response()->json($data);
    }
    function alamat(){
      $datan= Office::all();
      $baris = array();
      $a = 0 ;
      foreach ($datan as $datas) {
         $baris[$a]['id_off'] = $datas->id_off;
         $baris[$a]['nama'] = $datas->nama;
         $baris[$a]['alamat'] = $datas->alamat;  
         $baris[$a]['kota'] = $datas->kota;
         $baris[$a]['kodepos'] = $datas->kodepos; 
         $baris[$a]['koordinat'] = $datas->koordinat;  
         $baris[$a]['telp'] = $datas->telp;
         $baris[$a]['aktif'] = $datas->aktif;
      $a++;
       } 
       $data = array ('data'=>$baris);
       return response()->json($data);
    }
    function jps_pr(){
      $datan= Jenis::all();
      $baris = array();
      $a = 0 ;
      foreach ($datan as $datas) {
         $baris[$a]['id_jps'] = $datas->id_jps;
         $baris[$a]['jps'] = $datas->jps;
        
      $a++;
       } 
       $data = array ('data'=>$baris);
       return response()->json($data);
    }

    function linkvid(){
      $datan= Linkvideo::all();
      $baris = array();
      $a = 0 ;
      foreach ($datan as $datas) {
         $baris[$a]['id_vid'] = $datas->id_vid;
         $baris[$a]['judulvid'] = $datas->judulvid;
         $baris[$a]['linkvid'] = $datas->linkvid;
        
      $a++;
       } 
       $data = array ('data'=>$baris);
       return response()->json($data);
    }
    function sli(){
      $datan= Slider::all();
      // $baris = array();
      // $a = 0 ;
      // foreach ($datan as $datas) {
      //    $baris[$a]['id_sld'] = $datas->id_sld;
      //    $baris[$a]['judulsld'] = $datas->judulsld;
      //    $baris[$a]['subjudul'] = $datas->subjudul;
      //    $baris[$a]['prakata'] = $datas->prakata;
      //    $baris[$a]['deskripsi'] = $datas->deskripsi;
      //    $baris[$a]['posisi'] = $datas->posisi;
      //    $baris[$a]['urut'] = $datas->urut;
      //    $baris[$a]['aktif'] = $datas->aktif;
      //    $baris[$a]['lokasi'] = Slider::join('gbr', 'gbr.id_sld', '=', 'slider.id_sld')->select('gbr.lokasi')->get();
        
      // $a++;
      //  } 
      $row= Slider::join('gbr', 'gbr.id_sld', '=', 'slider.id_sld')->select('slider.*','gbr.lokasi')->get();
       $data = array ('data'=>$row);
      

       return response()->json($data);
    }
     
      function pro(){
    
      $produk= Produk::leftjoin('jns_prosrv','jns_prosrv.id_jps', '=', 'produk.id_jps')->get();
       $data = array ('data'=>$produk);
       return response()->json($data);
    }
    function prj(){
     
      $projek= Projek::leftjoin('jns_prosrv','jns_prosrv.id_jps', '=', 'projek.id_jps')->get();
       $data = array ('data'=>$projek);
       return response()->json($data);
    }
     function svs(){
     
      $servis= Service::join('jns_prosrv','jns_prosrv.id_jps', '=', 'servis.id_jps')->get();
       $data = array ('data'=>$servis);
       return response()->json($data);
    }
     function part(){
     
      $partner= Partner::leftjoin('gbr', 'gbr.id_partner', '=', 'partner.id_partner')->get();
       $data = array ('data'=>$partner);
       return response()->json($data);
    }
    function unduh(){
     
      $unduh= Unduhan::all();
       $data = array ('data'=>$unduh);
       return response()->json($data);
    }
    function usrweb(){
     
      $userweb= Userweb::leftjoin('gbr', 'gbr.id_usr', '=', 'usr_web.id_usr')->leftjoin('jns_usr', 'jns_usr.id_jns', '=', 'usr_web.jnsuser')->select('usr_web.*','jns_usr.*','gbr.lokasi')->get();
       $data = array ('data'=>$userweb);
       return response()->json($data);
    }
    function klientesti(){
     
      $klien = Klien::leftjoin('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->get();
       $data = array ('data'=>$klien);
       return response()->json($data);
    }
     function testiklien(){
     
      $testi = Testi::leftjoin('gbr', 'gbr.id_testi', '=', 'testi.id_testi')->leftjoin('klien','klien.id_klien', '=', 'testi.id_klien')->get();
       $data = array ('data'=>$testi);
       return response()->json($data);
    }
    function blogadmin(){

       $blog= Blog::leftjoin('usr_web', 'usr_web.id_usr', '=', 'blog.rec_usr')->select('blog.*', 'usr_web.namauser')->get();
        $data = array ('data'=>$blog);
       return response()->json($data);
    }
    function arraygambar(Request $req){
      $gbr = $req->get('id');
      $gbr = $req->id;
      $jenis = Jenis::all();
      $data['gambar'] = Gambar::where('id_prod', $gbr)->get();

      return view('template.PageProduct', $data, ['jenis'=>$jenis]);
    }

    public function addPost(Request $request){
      $rules = array(

        'jnskontak' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        'dtlkontak' =>  'required|min:2|max:12|regex:/^[0-9.]+$/',
        'ikon' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        'urut' =>  'required|min:1|max:12|regex:/^[0-9.]+$/',
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $kontak = new Kontak;
      $kontak->id_ktk = $request->id;
      $kontak->jnskontak = $request->jnskontak;
      $kontak->dtlkontak= $request->dtlkontak;
      $kontak->ikon = $request->ikon;
      $kontak->urut= $request->urut;
      $kontak->save();
      return response()->json($kontak);
    }
}

    public function editprof(request $request){
    $prof = Profile::find ($request->id_prof);
	  $prof->slogan = $request->slogan;
	  $prof->highlight = $request->highlight;
	  $prof->profil = $request->profil;
	  $prof->save();
	  return response()->json($prof);

}
public function editPost(request $request){
  $rules = array( 
  		  // 'jnskontak' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
      //   'dtlkontak' =>  'required|min:2|max:12|regex:/^[0-9.]+$/',
      //   'ikon' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
      //   'urut' =>  'required|min:1|max:12|regex:/^[0-9.]+$/',
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
  	  $kontak = Kontak::find ($request->id_ktk);
      $kontak->jnskontak = $request->jnskontak;
      $kontak->dtlkontak= $request->dtlkontak;
      $kontak->ikon = $request->ikon;
      $kontak->urut= $request->urut;
      $kontak->save();
  return response()->json($kontak);
}
}

public function deletePost(request $request){
  $kontak = Kontak::find ($request->id_ktk)->delete();
  return response()->json();
}

public function addSosmed(Request $request){
      $rules = array(

        'jnssosmed' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        'dtlsosmed' =>  'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        'ikonsosmed' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        'urutsosmed' =>  'required|min:1|max:12|regex:/^[0-9.]+$/',
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $sosmed = new Sosmed;
      $sosmed->id_sosmed = $request->id;
      $sosmed->jnssosmed = $request->jnssosmed;
      $sosmed->dtlsosmed= $request->dtlsosmed;
      $sosmed->ikon = $request->ikonsosmed;
      $sosmed->urut= $request->urutsosmed;
      $sosmed->save();
      return response()->json($sosmed);
    }
}
public function editmedia(request $request){
  $rules = array( 
        // 'jnssosmed' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'dtlsosmed' =>  'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'ikonsosmed' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'urutsosmed' =>  'required|min:1|max:12|regex:/^[0-9.]+$/',
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {

      $sosmed = Sosmed::find ($request->id_sosmed);
      $sosmed->jnssosmed = $request->jnssosmed;
      $sosmed->dtlsosmed= $request->dtlsosmed;
      $sosmed->ikon = $request->ikonsosmed;
      $sosmed->urut= $request->urutsosmed;
      $sosmed->save();
  return response()->json($sosmed);
}
}
public function deletemedia(request $request){
  $sosmed = Sosmed::find ($request->id_sosmed)->delete();
  return response()->json();
}
public function addAlamat(Request $request){
      $rules = array(

       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $office = new Office;
      $office->id_off = $request->id_off;
      $office->nama = $request->nama;
      $office->alamat= $request->alamat;
      $office->kota = $request->kota;
      $office->kodepos= $request->kodepos;
      $office->koordinat= $request->koordinat;
      $office->telp = $request->telp;
      $office->aktif= $request->aktif;
      $office->save();
      return response()->json($office);
    }
}
public function editoffice(request $request){
  $rules = array( 
        // 'jnssosmed' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'dtlsosmed' =>  'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'ikonsosmed' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'urutsosmed' =>  'required|min:1|max:12|regex:/^[0-9.]+$/',
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {

      $office = Office::find ($request->id_off);
      $office->nama = $request->nama;
      $office->alamat= $request->alamat;
      $office->kota = $request->kota;
      $office->kodepos= $request->kodepos;
      $office->koordinat= $request->koordinat;
      $office->telp = $request->telp;
      $office->aktif= $request->aktif;
      $office->save();
      return response()->json($office);
}
}
public function deleteoffice(request $request){
  $office = Office::find ($request->id_off)->delete();
  return response()->json();
}
public function tbljenis(Request $request){
      $rules = array(

        'jps' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $jenis = new Jenis;
      $jenis->id_jps = $request->id;
      $jenis->jps = $request->jps;
     
      $jenis->save();
      return response()->json($jenis);
    }
}
public function jpsedt(request $request){
  $rules = array( 
        // 'jnskontak' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
      //   'dtlkontak' =>  'required|min:2|max:12|regex:/^[0-9.]+$/',
      //   'ikon' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
      //   'urut' =>  'required|min:1|max:12|regex:/^[0-9.]+$/',
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $jenis = Jenis::find ($request->id_jps);
      $jenis->jps = $request->jps;
     
      $jenis->save();
  return response()->json($jenis);
}
}
public function deletejenis(request $request){
  $jenis = Jenis::find ($request->id_jps)->delete();
  return response()->json();
}

public function tblvd(Request $request){
      $rules = array(

        'judulvid' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        'linkvid' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $video = new Linkvideo;
      $video->id_vid = $request->id;
      $video->judulvid = $request->judulvid;
      $video->linkvid = $request->linkvid;
     
      $video->save();
      return response()->json($video);
    }
}
public function linkedt(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $video = Linkvideo::find ($request->id_vid);
      $video->judulvid = $request->judulvid;
      $video->linkvid = $request->linkvid;
     
      $video->save();
  return response()->json($video);
}
}
public function deletevideo(request $request){
  $video = Linkvideo::find ($request->id_vid)->delete();
  return response()->json();
}

public function tblsli(Request $request){
      $rules = array(

        // 'judulsld' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'subjudul' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'prakata' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'deskripsi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'posisi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'urut' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'aktif' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $slider = new Slider;
      $slider->id_sld = $request->id;
      $slider->judulsld = $request->judulsld;
      $slider->subjudul = $request->subjudul;
      $slider->prakata = $request->prakata;
      $slider->deskripsi = $request->deskripsi;
      $slider->posisigambar = $request->posisigambar;
      $slider->posisi = $request->posisiteks;
      $slider->urut = $request->urut;
      $slider->aktif = $request->aktif;

      $gbr= new Gambar;
      $gbr->id_sld = $slider->id;
     
      $slider->save();
      $slider->gambar()->save($gbr);

      return response()->json($slider);
    }
}

public function slidedt(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $slider = Slider::find ($request->id);
      $slider->judulsld = $request->judulsld;
      $slider->subjudul = $request->subjudul;
      $slider->prakata = $request->prakata;
      $slider->deskripsi = $request->deskripsi;
      $slider->posisigambar = $request->posisigambar;
      $slider->posisi = $request->posisiteks;
      $slider->urut = $request->urut;
      $slider->aktif = $request->aktif;
     
      $slider->save();
  return response()->json($slider);
}
}
public function deleteslider(request $request){
  $slider = Slider::find ($request->id_sld)->delete();
  return response()->json();
}

public function tambahproduk(Request $request){
      $rules = array(

        // 'judulsld' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'subjudul' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'prakata' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'deskripsi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'posisi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'urut' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'aktif' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $produk = new Produk;
      $produk->id_prod = $request->id;
      $produk->id_jps = $request->jps;
      $produk->produk = $request->produk;
      $produk->deskripsi = $request->deskripsi;
    
      $gbr= new Gambar;
      $gbr->id_prod = $produk->id;
     
      $produk->save();
      $produk->gambar()->save($gbr);

      return response()->json($produk);
    }
}
public function editproduk(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $produk = Produk::find ($request->id_prod);
      $produk->id_jps = $request->jps;
      $produk->produk = $request->produk;
      $produk->deskripsi = $request->deskripsi;
     
      $produk->save();
  return response()->json($produk);
}
}
public function hapusproduk(request $request){
  $produk = Produk::find ($request->id_prod)->delete();
  return response()->json();
}
public function tambahprojek(Request $request){
      $rules = array(

        // 'judulsld' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'subjudul' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'prakata' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'deskripsi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'posisi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'urut' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'aktif' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $projek = new Projek;
      $projek->id_prjt = $request->id;
      $projek->id_jps = $request->jps;
      $projek->projek = $request->projek;
      $projek->deskripsi = $request->deskripsi;
    
      $gbr= new Gambar;
      $gbr->id_prjt = $projek->id;
     
      $projek->save();
      $projek->gambar()->save($gbr);

      return response()->json($projek);
    }
}
public function editprojek(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $projek = Projek::find ($request->id_prjt);
      $projek->id_jps = $request->jps;
      $projek->projek = $request->projek;
      $projek->deskripsi = $request->deskripsi;
     
      $projek->save();
  return response()->json($projek);
}
}
public function hapusprojek(request $request){
  $Projek = Projek::find ($request->id_prjt)->delete();
  return response()->json();
}
public function tambahservis(Request $request){
      $rules = array(

        // 'judulsld' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'subjudul' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'prakata' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'deskripsi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'posisi' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'urut' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'aktif' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $servis = new Service;
      $servis->id_serv = $request->id;
      $servis->id_jps = $request->jps;
      $servis->servis = $request->servis;
      $servis->ikon = $request->ikon;
    
      
      $servis->save();
      return response()->json($servis);
    }
}
public function editservis(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $servis = Service::find ($request->id_serv);
      $servis->id_jps = $request->jps;
      $servis->servis = $request->servis;
      $servis->ikon = $request->ikon;
     
      $servis->save();
  return response()->json($servis);
}
}
public function hapusservis(request $request){
  $servis = Service::find ($request->id_serv)->delete();
  return response()->json();
}
public function tblunduh(Request $request){
      $rules = array(

        // 'judulunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'linkunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $unduh = new Unduhan;
      $unduh->id_unduh = $request->id;
      $unduh->judulunduh = $request->judulunduh;
      $unduh->linkunduh = $request->linkunduh;
     
      $unduh->save();
      return response()->json($unduh);
    }
}
public function editunduhan(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $unduh = Unduhan::find ($request->id_unduh);
      $unduh->id_unduh = $request->id_unduh;
      $unduh->judulunduh = $request->judulunduh;
      $unduh->linkunduh = $request->linkunduh;
     
      $unduh->save();
  return response()->json($unduh);
}
}
public function hapusunduh(request $request){
  $unduh = Unduhan::find ($request->id_unduh)->delete();
  return response()->json();
}
public function tbluser(Request $request){
      $rules = array(

        // 'judulunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'linkunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $userweb = new Userweb;
      $userweb->id_usr= $request->id_usr;
      $userweb->namauser = $request->namauser;
      $userweb->email = $request->email;
      $userweb->jnsuser = $request->jenisuser;
      $userweb->status = $request->status;
      $gbr= new Gambar;
      $gbr->id_usr = $userweb->id;
     
      $userweb->save();
      $userweb->gambar()->save($gbr);
      return response()->json($userweb);
    }
}
public function edituser(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $userweb = Userweb::find ($request->id_usr);
      $userweb->id_usr = $request->id_usr;
      $userweb->namauser = $request->namauser;
      $userweb->email = $request->email;
      $userweb->jnsuser = $request->jenisuser;
      $userweb->status = $request->status;
     
      $userweb->save();
  return response()->json($userweb);
}
}
public function hapususer(request $request){
  $userweb = Userweb::find ($request->id_usr)->delete();
  return response()->json();
}
public function tblblog(Request $request){
      $rules = array(

        // 'judulunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'linkunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $bl = new Blog;
      $bl->id_blog = $request->id_blog;
      $bl->judul = $request->judul;
      $bl->rec_usr = $request->namauser;
      $gbr= new Gambar;
      $gbr->id_blog = $bl->id;
     
      $bl->save();
      $bl->gambar()->save($gbr);
      return response()->json($bl);
    }
}
public function editblog(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $bl = Blog::find ($request->id_blog);
      $bl->id_blog = $request->id_blog;
      $bl->judul = $request->judul;
      $bl->rec_usr = $request->rec_usr;
     
      $bl->save();
  return response()->json($bl);
}
}
public function hapusblog(request $request){
  $bl = Blog::find ($request->id_blog)->delete();
  return response()->json();
}
public function tblklien(Request $request){
      $rules = array(

        // 'judulunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'linkunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $klien = new Klien;
      $klien->id_klien= $request->id_klien;
      $klien->klien = $request->klien;
      $klien->alamat = $request->alamat;
      $klien->telp = $request->telp;
      $klien->email = $request->email;
      $gbr= new Gambar;
      $gbr->id_klien = $klien->id;
     
      $klien->save();
      $klien->gambar()->save($gbr);
      return response()->json($klien);
    }
}
public function editklien(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $klien = Klien::find ($request->id_klien);
      $klien->id_klien = $request->id_klien;
      $klien->klien = $request->klien;
      $klien->alamat = $request->alamat;
      $klien->telp = $request->telp;
      $klien->email = $request->email;
     
     
      $klien->save();
  return response()->json($klien);
}
}
public function hapusklien(request $request){
  $klien = Klien::find ($request->id_klien)->delete();
  return response()->json();
}
public function tbltesti(Request $request){
      $rules = array(

        // 'judulunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'linkunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $testi = new Testi;
      $testi->id_klien = $request->klien;
      $testi->nama = $request->nama;
      $testi->testimoni = $request->testimoni;
     
      $gbr= new Gambar;
      $gbr->id_testi = $testi->id;
     
      $testi->save();
      $testi->gambar()->save($gbr);
      return response()->json($testi);
    }
}

public function edittesti(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $testi = Testi::find ($request->id_testi);
      $testi->id_testi = $request->id_testi;
      $testi->id_klien = $request->klien;
      $testi->nama = $request->nama;
      $testi->testimoni = $request->testimoni;
     
      $testi->save();
  return response()->json($testi);
}
}
public function hapustesti(request $request){
  $testi = Testi::find ($request->id_testi)->delete();
  return response()->json();
}
public function tblpartner(Request $request){
      $rules = array(

        // 'judulunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
        // 'linkunduh' => 'required|min:2|max:300|regex:/^[a-z ,.\'-]+$/i',
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    else {
      $partner = new Partner;
      $partner->id_partner= $request->id_partner;
      $partner->partner = $request->partner;
      $partner->alamat = $request->alamat;
      $partner->telp = $request->telp;
      $partner->email = $request->email;
     
      $gbr= new Gambar;
      $gbr->id_partner = $partner->id;
     
      $partner->save();
      $partner->gambar()->save($gbr);
      return response()->json($partner);
    }
}

public function editpartner(request $request){
  $rules = array( 
       
      );
    $validator = Validator::make ( Input::all(), $rules);
    if ($validator->fails())
    return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

     else {
      $partner = Partner::find ($request->id_partner);
      $partner->id_partner = $request->id_partner;
      $partner->partner = $request->partner;
      $partner->alamat = $request->alamat;
      $partner->telp = $request->telp;
      $partner->email = $request->email;
     
      $partner->save();
  return response()->json($partner);
}
}
public function hapuspartner(request $request){
  $partner = Partner::find ($request->id_partner)->delete();
  return response()->json();
}





//upload image
 function uploadSlid(Request $req){
  $text = $req->picsld;
  $gbr = $req->file('picturesls');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->picturesls->move(public_path('img/slides'), $name);
  $up = Gambar::where('id_sld', $req->picsld);
  $up->update(['lokasi'=>'img/slides/'.$name]);
  // return response()->json($up);
      $profile = Profile::all();
      $userweb = Userweb::all();
      $klien = Klien::all();
      $jenis = Jenis::all();
      $user = Userweb::all();
      $kontak = Kontak::all();
      $sosmed = Sosmed::all();
      $office = Office::all();
      $gambar = Gambar::all();
      $produk = Produk::all();
      $jenisuser = JenisUser::all();
      $blog = Blog::all();
      $projek = Projek::all();
      $slider = Slider::join ('gbr','gbr.id_sld', '=', 'slider.id_sld')->select('gbr.*', 'slider.*')->orderby('urut')->get();

      // return view('template.inisialisasi', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'gambar'=>$gambar,'jenis'=>$jenis,'sosmed'=>$sosmed,'kontak'=>$kontak,'profile'=>$profile,'office'=>$office,'slider'=>$slider]);
      return redirect()->action('GritController@inisialisasi');
 }
  function uploadProd(Request $req){
  $text = $req->picprd;
  $gbr = $req->file('pictureprdk');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->pictureprdk->move(public_path('img/showcase'), $name);
  // $up = Gambar::where('id_prod', $req->picprd);
  $up = new Gambar;
  $up ->lokasi = 'img/showcase/'.$name;
  $up->id_prod = $text;
  $up->save();
  // $up->create(['lokasi'=>'img/showcase/'.$name]);
  // return response()->json($up);
      $jenis = Jenis::all();
      $gambar = Gambar::all();
      $produk = Produk::all();
      $user = Userweb::all();
      $userweb = Userweb::all();
      $klien= Klien::all();
      $blog = Blog::all();
      $projek = Projek::all();
      $jenisuser = JenisUser::all();
      // $produk = Produk::join ('gbr','gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get();
      // return view('template.PageProduct', ['jenisuser'=>$jenisuser,'user'=>$user,'klien'=>$klien,'userweb'=>$userweb,'jenis'=>$jenis,'produk'=>$produk,'gambar'=>$gambar]);
      return redirect()->action('GritController@pageproduk');
 }
 function uploadPrj(Request $req){
  $text = $req->picprj;
  $gbr = $req->file('pictureprjk');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->pictureprjk->move(public_path('img/projek'), $name);
   $up = new Gambar;
   $up->lokasi = 'img/projek/'.$name;
   $up->id_prjt = $text;
   $up->save();

  // $up = Gambar::where('id_prjt', $req->picprj);
  // $up->update(['lokasi'=>'img/projek/'.$name]);
  // return response()->json($up);
   $jenis = Jenis::all();
   $projek = Projek::all();
   $gambar = Gambar::all();
   $produk = Produk::all();
   $userweb = Userweb::all();
   $klien = Klien::all();
   $user = Userweb::all();
   $jenisuser = JenisUser::all();
   $blog= Blog::all();
   // return view('template.PageProject', ['blog'=>$blog,'jenisuser'=>$jenisuser,'klien'=>$klien,'userweb'=>$userweb,'user'=>$user,'produk'=>$produk,'gambar'=>$gambar,'jenis'=>$jenis,'projek'=>$projek]);
   return redirect()->action('GritController@pageprojek');
 }

function uploadUser(Request $req){
  $text = $req->picusr;
  $gbr = $req->file('pictureuser');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->pictureuser->move(public_path('img/userweb'), $name);
  $up = Gambar::where('id_usr', $req->picusr);
  $up->update(['lokasi'=>'img/userweb/'.$name]);
  // return response()->json($up);
   $userweb = Userweb::all();
   $userweb = Userweb::all();
   $jenis = Jenis::all();
   $gambar = Gambar::all();
   $produk = Produk::all();
   $klien =Klien::all();
   $user = Userweb::all();
   $jenisuser = JenisUser::all();
   $blog = Blog::all();
   $projek = Projek::all();
    // return view('template.PageUserweb', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'jenis'=>$jenis,'gambar'=>$gambar,'userweb'=>$userweb]);
   return redirect()->action('GritController@pageuserweb');
 }

function uploadKlien(Request $req){
  $text = $req->pickln;
  $gbr = $req->file('pictureklien');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->pictureklien->move(public_path('img/clients'), $name);
  $up = Gambar::where('id_klien', $req->pickln);
  $up->update(['lokasi'=>'img/clients/'.$name]);
  // return response()->json($up);
  $jenis = Jenis::all();
  $gambar = Gambar::all();
  $produk = Produk::all();
  $userweb = Userweb::all();
  $klien = Klien::all();
  $testi = Testi::all();
  $user = Userweb::all();
  $blog = Blog::all();
  $jenisuser = JenisUser::all();
  $projek = Projek::all();
  // return view('template.PageKlienTesti', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'testi'=>$testi,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'jenis'=>$jenis,'gambar'=>$gambar]);
  return redirect()->action('GritController@pageklientesti');
 }

function uploadTesti(Request $req){
  $text = $req->pictst;
  $gbr = $req->file('picturetesti');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->picturetesti->move(public_path('img/misc'), $name);
  $up = Gambar::where('id_testi', $req->pictst);
  $up->update(['lokasi'=>'img/misc/'.$name]);
  // return response()->json($up);
  $jenis = Jenis::all();
  $gambar = Gambar::all();
  $produk = Produk::all();
  $userweb = Userweb::all();
  $klien = Klien::all();
  $testi = Testi::all();
  $user = Userweb::all();
  $jenisuser = JenisUser::all();
  $blog = Blog::all();
  $projek = Projek::all();
  // return view('template.PageKlienTesti', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'user'=>$user,'testi'=>$testi,'klien'=>$klien,'userweb'=>$userweb,'produk'=>$produk,'jenis'=>$jenis,'gambar'=>$gambar]);
  return redirect()->action('GritController@pageklientesti');
 }

function uploadPartner(Request $req){
  $text = $req->picprt;
  $gbr = $req->file('picturepartner');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->picturepartner->move(public_path('img/partner'), $name);
  $up = Gambar::where('id_partner', $req->picprt);
  $up->update(['lokasi'=>'img/partner/'.$name]);
  // return response()->json($up);

  $jenis = Jenis::all();
  $produk = Produk::all();
  $partner = Partner::all();
  $userweb = Userweb::all();
  $gambar = Gambar::all();
  $user = Userweb::all();
  $klien = Klien::all();
  $jenisuser = JenisUser::all();
  $blog = Blog::all();
  $projek = Projek::all();
  // return view('template.PagePartner', ['projek'=>$projek,'blog'=>$blog,'jenisuser'=>$jenisuser,'gambar'=>$gambar,'user'=>$user,'produk'=>$produk,'userweb'=>$userweb,'klien'=>$klien,'jenis'=>$jenis,'partner'=>$partner]);
   return redirect()->action('GritController@pagepartner');
 }

 function uploadBlog(Request $req){
  $text = $req->picblg;
  $gbr = $req->file('pictureblog');
  $name = time().'.'.$gbr->getClientOriginalExtension();
  $req->pictureblog->move(public_path('img/blog'), $name);
  // $up = Gambar::where('id_blog', $req->picblg);
  // $up->create(['lokasi'=>'img/blog/'.$name]);
  // return response()->json($up);
   $up = new Gambar;
   $up ->lokasi = 'img/blog/'.$name;
   $up->id_blog = $text;
   $up->save();

      $jenis = Jenis::all();
      $gambar = Gambar::all();
      $blog = Blog::all();
      $produk = Produk::all();
      $userweb = Userweb::all();
      $klien = Klien::all();
      $user = Userweb::all();
      $projek = Projek::all();
      $jenisuser = JenisUser::all();
      return redirect()->action('GritController@pageblog');
 }



 //hapus gambar
public function hapuspic(request $request){
  $gambar = Gambar::find ($request->id_gbr)->delete();
  return response()->json();
}
// public function hapuspicuser(request $request){
//   $gbrur = Gambar::find ($request->id_gbr)->delete();
//   return response()->json();
// }
//login
public function __construct()
    {
        $this->middleware('auth');
    }
public function changePassword(Request $request){
 
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
 
        // $validatedData = $request->validate([
        //     'current-password' => 'required',
        //     'new-password' => 'required|string|min:6|confirmed',
        // ]);
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
 
        return redirect()->back()->with("success","Password changed successfully !");
 
    }

public function showChangePasswordForm(){
        return view('auth.changepassword');
    }









}