<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kontak;
use App\Slider;
use App\Sosmed;
use App\Gambar;
use App\Produk;
use App\Klien;
use App\Blog;
use App\Service;
use App\Profile;
use App\Testi;
use App\Office;
use App\Jenis;
use App\Linkvideo;

class ViewController extends Controller
{
    public function about(){
    $klien = Klien::join ('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
    $sosmed = Sosmed::all();
    $kontak = Kontak::all();
    $gambar = Gambar::all();
    $profile = Profile::join('gbr', 'gbr.id_prof', '=', 'profile.id_prof')->select('gbr.*', 'profile.*')->get();
      return view('template.about', ['sosmed'=>$sosmed,'kontak'=>$kontak,'klien'=>$klien,'profile'=>$profile]);
   

    }
      function baru($id){
        $klien = Klien::join ('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
      $sosmed = Sosmed::all();
      $kontak = Kontak::all();
      $gambar = Gambar::all();
      $blog = Blog::find($id);
      $bloge = Blog::join('usr_web', 'usr_web.id_usr', '=', 'blog.rec_usr')->select('usr_web.*', 'blog.*')->get();
      return view('template.blog', ['sosmed'=>$sosmed,'kontak'=>$kontak,'klien'=>$klien,'blog'=>$blog,'bloge'=>$bloge]);
    }

     public function element(){
     
      return view('template.element');
    }
     public function login(){
     
      return view('template.login');
    }
     public function regis(){
     
      return view('template.regis');
    }



  public function sosmed()
    {
    $sosmed = Sosmed::all();
    $kontak = Kontak::all();
    $servis = Service::orderby('rec_wkt', 'desc')->get()->shuffle();
    $gambar = Gambar::all(); 
    $testi = Testi::join ('gbr', 'gbr.id_testi', '=', 'testi.id_testi')->join('klien', 'klien.id_klien', '=', 'testi.id_klien')->get()->shuffle();
    $slider = Slider::join ('gbr','gbr.id_sld', '=', 'slider.id_sld')->select('gbr.*', 'slider.*')->orderby('urut')->get();
    // $produk = Produk::all()->shuffle();

    $produk = Produk::join ('gbr', 'gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get()->shuffle();
    // $produk = Produk::join ('gbr', 'gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get()->shuffle();
    $klien = Klien::join ('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
    $blog = Blog::orderby('rec_wkt', 'desc')->get();
    $profile = Profile::all();
   return view('template.pageheader', ['sosmed'=>$sosmed,'kontak'=>$kontak,'slider'=>$slider,'produk'=>$produk,'klien'=>$klien,'blog'=>$blog,'servis'=>$servis,'profile'=>$profile,'testi'=>$testi]);
    }



}
