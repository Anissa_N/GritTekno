<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
     protected $table = 'slider';
     protected $primaryKey = 'id_sld';
     public $timestamps = false;
     function gambar(){
     	return $this->hasOne('App\Gambar', 'id_sld');
     }

}
