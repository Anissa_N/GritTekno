<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testi extends Model
{
    protected $table = 'testi';
     protected $primaryKey = 'id_testi';
     public $timestamps = false;
       function gambar(){
     	return $this->hasOne('App\Gambar', 'id_testi');
     }
}
