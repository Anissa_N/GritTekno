<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisUser extends Model
{
    protected $table = 'jns_usr';
     protected $primaryKey = 'id_jns';
     public $timestamps = false;

}
