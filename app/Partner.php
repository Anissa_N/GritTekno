<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = 'partner';

     protected $primaryKey = 'id_partner';
     public $timestamps = false;

    public function gambar(){
     	return $this->hasMany('App\Gambar', 'id_partner');
     }
}
