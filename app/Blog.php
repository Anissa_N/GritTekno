<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
     protected $table = 'blog';
     protected $primaryKey = 'id_blog';
     public $timestamps = false;
       function gambar(){
     	return $this->hasMany('App\Gambar', 'id_blog');
     }
}
