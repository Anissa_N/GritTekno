<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
     protected $table = 'servis';
      protected $primaryKey = 'id_serv';
     public $timestamps = false;
    
}
