<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontak extends Model
{
     protected $table = 'kontak';
     protected $primaryKey = 'id_ktk';
     public $timestamps = false;
     
}
