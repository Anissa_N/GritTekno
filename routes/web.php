<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('pageheader','ViewController@sosmed');
Route::get('aboutpage','ViewController@about');
Route::get('elementpage','ViewController@element');
Route::get('blogpage/{id}','ViewController@baru');

Route::get('regis','GritController@regis');
//pageview
Route::get('login','GritController@login');
Route::get('admin','GritController@admn');
Route::get('inisial','GritController@inisialisasi');
Route::get('produk','GritController@pageproduk');
Route::get('projek','GritController@pageprojek');
Route::get('servis','GritController@pageservis');
Route::get('partner','GritController@pagepartner');
Route::get('unduhan','GritController@pageunduh');
Route::get('klientesti','GritController@pageklientesti');
Route::get('blogadmin','GritController@pageblog');
Route::get('userweb','GritController@pageuserweb');

Route::get('pictured','GritController@arraygambar');
//datatables
Route::get('data','GritController@bebas');
Route::get('datamedsos','GritController@media');
Route::get('alam','GritController@alamat');
Route::get('jenis','GritController@jps_pr');
Route::get('linkvid','GritController@linkvid');
Route::get('dataslider','GritController@sli');
Route::get('produkpage','GritController@pro');
Route::get('projekpage','GritController@prj');
Route::get('servispage','GritController@svs');
Route::get('unduhpage','GritController@unduh');
Route::get('partnerpage','GritController@part');
Route::get('blogpage','GritController@blogadmin');
Route::get('userwebpage','GritController@usrweb');
Route::get('klienpage','GritController@klientesti');
Route::get('testipage','GritController@testiklien');



Route::post('addPost','GritController@addPost');
Route::post('editkon','GritController@editPost');
Route::post('delkon','GritController@deletePost');
Route::post('addsosmed','GritController@addSosmed');
Route::post('editsosmed','GritController@editmedia');
Route::post('delsos','GritController@deletemedia');
Route::post('tambahalamat','GritController@addAlamat');
Route::post('ubahalamat','GritController@editoffice');
Route::post('hapusalamat','GritController@deleteoffice');
Route::post('addjenis','GritController@tbljenis');
Route::post('editjenis','GritController@jpsedt');
Route::post('deljns','GritController@deletejenis');
Route::post('addvideo','GritController@tblvd');
Route::post('video','GritController@linkedt');
Route::post('delvid','GritController@deletevideo');

Route::post('addslid','GritController@tblsli');
Route::patch('sliderubah/{id}','GritController@slidedt');
Route::post('delslid','GritController@deleteslider');
Route::post('addprd','GritController@tambahproduk');
Route::patch('editprd/{id}','GritController@editproduk');
Route::post('delprd','GritController@hapusproduk');
Route::post('addprj','GritController@tambahprojek');
Route::patch('editprjk/{id}','GritController@editprojek');
Route::post('delprjk','GritController@hapusprojek');
Route::post('addservis','GritController@tambahservis');
Route::patch('editserv/{id}','GritController@editservis');
Route::post('delserv','GritController@hapusservis');

Route::post('addunduh','GritController@tblunduh');
Route::patch('ubahunduhan/{id}','GritController@editunduhan');
Route::post('delunduh','GritController@hapusunduh');
Route::post('adduser','GritController@tbluser');
Route::patch('ubahuser/{id}','GritController@edituser');
Route::post('deluser','GritController@hapususer');

Route::post('addblog','GritController@tblblog');
Route::patch('editblog/{id}','GritController@editblog');
Route::post('delblog','GritController@hapusblog');
Route::post('addklien','GritController@tblklien');
Route::patch('editklien/{id}','GritController@editklien');
Route::post('delklien','GritController@hapusklien');

Route::post('addtesti','GritController@tbltesti');
Route::patch('edittesti/{id}','GritController@edittesti');
Route::post('deltesti','GritController@hapustesti');
Route::post('addpartner','GritController@tblpartner');
Route::patch('editpartner/{id}','GritController@editpartner');
Route::post('delpartner','GritController@hapuspartner');



Route::post('delgbr','GritController@hapuspic');


//upload Gambar
Route::post('/uploadgambarsld','GritController@uploadSlid');
Route::post('/uploadgambarproduk','GritController@uploadProd');
Route::post('/uploadgambarprojek','GritController@uploadPrj');
Route::post('/uploadgambarusr','GritController@UploadUser');
Route::post('/uploadgambarpartner','GritController@uploadPartner');
Route::post('/uploadgambartesti','GritController@UploadTesti');
Route::post('/uploadgambarklien','GritController@uploadKlien');
Route::post('/uploadgambarblog','GritController@uploadBlog');


Route::patch('profile','GritController@editprof');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/changePassword','GritController@showChangePasswordForm'); 


Route::post('/changePassword','GritController@changePassword')->name('changePassword');
 
